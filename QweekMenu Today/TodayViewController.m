//
//  TodayViewController.m
//  QweekMenu Today
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "General.h"

@interface TodayViewController () <NCWidgetProviding>
@property (weak, nonatomic) IBOutlet UILabel *lunchLabel;
@property (weak, nonatomic) IBOutlet UILabel *dinnerLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    [self updateTodayMenu];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userDefaultsDidChange:)
                                                     name:NSUserDefaultsDidChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)configureView {
    self.preferredContentSize = CGSizeMake(self.view.frame.size.width, 110);
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openQweekMenu)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)userDefaultsDidChange:(NSNotification *)notification {
    [self updateTodayMenu];
}

- (void)updateTodayMenu {

    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:WIDGET_GROUP_NAME];
    NSArray *weekMenu = [defaults objectForKey:WIDGET_SUMMARY_KEY];
    
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *components = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                                fromDate: today];
    NSDate *dateZero = [gregorian dateFromComponents: components];
    
    
    BOOL found = NO;
    
    for (NSDictionary *dict in weekMenu) {
        NSDate *date = [dict objectForKey:@"date"];
        if([date isEqual:dateZero]){
            found = YES;
            [self showMenuSummaryTextWithDictionary:dict];
            break;
        }
    }
    
    if (!found){
        self.lunchLabel.text = NSLocalizedString(@"noDishesSummary", nil);
        self.preferredContentSize = CGSizeMake(self.view.frame.size.width, 50);
    }
    
    
}

- (void)showMenuSummaryTextWithDictionary:(NSDictionary *)dict {
    NSString *lunch = [dict objectForKey:@"lunch"];
    NSString *dinner = [dict objectForKey:@"dinner"];
    NSString *lunchName = [NSLocalizedString(@"lunch", nil) capitalizedString];
    NSString *dinnerName = [NSLocalizedString(@"dinner", nil) capitalizedString];
    self.lunchLabel.text = [NSString stringWithFormat:@"%@: %@",lunchName ,lunch];
    self.dinnerLabel.text = [NSString stringWithFormat:@"%@: %@",dinnerName, dinner];
    self.infoLabel.text = NSLocalizedString(@"enjoyTheMeal", nil);
}

- (void)openQweekMenu {
    NSExtensionContext *myExtension=[self extensionContext];
    [myExtension openURL:[NSURL URLWithString:@"qweekmenu://"] completionHandler:nil];
}

@end
