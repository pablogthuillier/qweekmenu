//
//  PGTAlerts.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 21/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTAlerts : NSObject

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
           confirmButtonTitle:(NSString *)confirmButtonTitle
              completionBlock:(void (^)(void))completionBlock;

- (void)showAlert;

@end
