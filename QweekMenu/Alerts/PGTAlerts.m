//
//  PGTAlerts.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 21/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTAlerts.h"

@interface PGTAlerts () <UIAlertViewDelegate>

@property (nonatomic,strong) UIAlertView *alert;
@property (nonatomic,copy) void (^completionBlock)(void);

@end

@implementation PGTAlerts



- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
           confirmButtonTitle:(NSString *)confirmButtonTitle
              completionBlock:(void (^)(void))completionBlock
{
    self = [super init];
    if (self) {
        _alert = [[UIAlertView alloc] initWithTitle:title
                                            message:message
                                           delegate:self
                                  cancelButtonTitle:cancelButtonTitle
                                  otherButtonTitles:confirmButtonTitle, nil];
        
        _completionBlock = completionBlock;
        
        [_alert show];
    }
    return self;
}

- (void)showAlert {
    [self.alert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        self.completionBlock();
    }
    
}


@end
