//
//  PGTEditDishTVC.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 17/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGTDishViewModel.h"

@class PGTEditDishTVC;


@protocol PGTEditDishTVCDelegate <NSObject>

- (void)dismissEditViewController:(PGTEditDishTVC *)editVC modifiedData:(BOOL)modifiedData;

@end

@interface PGTEditDishTVC : UITableViewController

@property (nonatomic,strong) UIManagedDocument *managedDocument;

@property (nonatomic,weak) id<PGTEditDishTVCDelegate> delegate;
@property (nonatomic,strong) PGTDishViewModel *viewModel;


@end
