//
//  PGTChooseDishViewController.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dish+Model.h"
#import "PGTDishesCollectionViewModel.h"

@class PGTChooseDishViewController;
extern NSString *const chooseDishVCName;

@protocol PGTChooseDishDelegate <NSObject>

- (void)dismissVC:(PGTChooseDishViewController *)controller withDish:(Dish *)dish inDayMomentName:(NSString *)dayMomentName;

@end

@interface PGTChooseDishViewController : UIViewController

@property (nonatomic,weak) id<PGTChooseDishDelegate> delegate;
@property (nonatomic,strong) PGTDishesCollectionViewModel *viewModel;
@property (nonatomic,strong) Dish *selectedDish;
@property (nonatomic,copy) NSString *dayMomentName;

@end
