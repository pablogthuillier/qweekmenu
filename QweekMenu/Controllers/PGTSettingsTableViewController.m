//
//  PGTSettingsTableViewController.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTSettingsTableViewController.h"


@interface PGTSettingsTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *meatIndicator;
@property (weak, nonatomic) IBOutlet UILabel *cerealsIndicator;
@property (weak, nonatomic) IBOutlet UIStepper *meatStepper;
@property (weak, nonatomic) IBOutlet UIStepper *cerealsStepper;
@property (weak, nonatomic) IBOutlet UISwitch *legumesSwitch;

@property (nonatomic,copy) NSArray *sectionSettings;

@end

@implementation PGTSettingsTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];

}

- (void)viewWillAppear:(BOOL)animated{
    [self setupView];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)setupView{
    self.sectionSettings = @[NSLocalizedString(@"repetitionsTitle", nil),NSLocalizedString(@"customTitle", nil)];
    [self setSettingsValues];
}

- (void)setSettingsValues{
    NSNumber *meatValue = [[NSUserDefaults standardUserDefaults] valueForKey:SETTINGS_MEAT_MAX_NAME];
    NSNumber *cerealsValue = [[NSUserDefaults standardUserDefaults] valueForKey:SETTINGS_CEREALS_MAX_NAME];
    NSNumber *legumesValue = [[NSUserDefaults standardUserDefaults] valueForKey:SETTINGS_LEGUMES_NAME];
    
    self.meatIndicator.text = [NSString stringWithFormat:@"%d",[meatValue intValue]];
    self.meatStepper.value = [meatValue doubleValue];
    
    self.cerealsIndicator.text = [NSString stringWithFormat:@"%d",[cerealsValue intValue]];
    self.cerealsStepper.value = [cerealsValue doubleValue];
    
    if([legumesValue isEqual:@1]){
        [self.legumesSwitch setOn:YES];
    }
    else{
        [self.legumesSwitch setOn:NO];
    }
    
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    return [self.sectionSettings objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.text = [tableViewHeaderFooterView.textLabel.text capitalizedString];
        tableViewHeaderFooterView.textLabel.font = [UIFont boldSystemFontOfSize:17];
        tableViewHeaderFooterView.textLabel.textColor = [UIColor blackColor];
        CALayer *bottomBorder = [CALayer layer];
        
        bottomBorder.frame = CGRectMake(0.0f, 0.0f, tableViewHeaderFooterView.frame.size.width, 0.8f);
        
        bottomBorder.backgroundColor = [UIColor colorWithWhite:1.0f
                                                         alpha:1.0f].CGColor;
        
        [tableViewHeaderFooterView.layer addSublayer:bottomBorder];
    }
}

- (IBAction)changeLegumesValue:(UISwitch *)sender {
    NSNumber *senderValue = [sender isOn] ? @1 : @0;
    [[NSUserDefaults standardUserDefaults] setObject:senderValue forKey:SETTINGS_LEGUMES_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

- (IBAction)changeMeatStepper:(UIStepper *)sender {
    int meatInt = (int)sender.value;
    self.meatIndicator.text = [NSString stringWithFormat:@"%d",meatInt];
    
    NSNumber *senderValue = [NSNumber numberWithDouble:sender.value];
    [[NSUserDefaults standardUserDefaults] setObject:senderValue forKey:SETTINGS_MEAT_MAX_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)changeCerealsStepper:(UIStepper *)sender {
    int cerealInt = (int)sender.value;
    self.cerealsIndicator.text = [NSString stringWithFormat:@"%d",cerealInt];
    
    NSNumber *senderValue = [NSNumber numberWithDouble:sender.value];
    [[NSUserDefaults standardUserDefaults] setObject:senderValue forKey:SETTINGS_CEREALS_MAX_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
