//
//  PGTDishesTableViewController.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDishesTableViewController.h"
#import "DishDatabaseAvailability.h"
#import "Dish+Model.h"
#import "PGTDishTableViewCell.h"
#import "PGTDishViewModel.h"
#import "MBProgressHUD.h"
#import "PGTFeedbackDishesView.h"
#import "PGTImportedDishesViewController.h"
#import "UIViewController+KNSemiModal.h"
#import "PGTCustomColoredAccesory.h"


NSString *const createSegueName = @"createDishSegue";
NSString *const editSegueName = @"editDishSegue";
NSString *const dishCellName = @"dishCell";
NSString *const feedbackDishViewName = @"PGTFeedbackDishesView";

@interface PGTDishesTableViewController () <PGTEditDishTVCDelegate, PGTImportedDishDelegate>

@property (nonatomic,copy) NSArray *ingredients;
@property (nonatomic,strong) PGTFeedbackDishesView *feedbackView;
@property (nonatomic,strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation PGTDishesTableViewController

@dynamic fetchedResultsController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self notificationWhenMOCChanges];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupView];
    [self initializeModel];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor colorWithHexString:COLOR_NEUTRAL];
    

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.feedbackView show:NO animated:NO];
    [self removeNotifications];
}

- (PGTFeedbackDishesView *)feedbackView{
    if(!_feedbackView){
        _feedbackView = [[[NSBundle mainBundle] loadNibNamed:feedbackDishViewName owner:self options:nil] objectAtIndex:0];
        [self.feedbackView setupWithTip1:NSLocalizedString(@"tipDishes1Text", @"feedBackTutorial")
                                 andTip2:nil];
        [self.feedbackView show:NO animated:NO];
        self.feedbackView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-49);
        
    }
    
    return _feedbackView;
}

- (void)setupView {
    if(!self.feedbackView.superview){
        [self.tableView addSubview:self.feedbackView];
        
    }
    
    
}


#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PGTDishTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dishCellName];
    
    Dish *dish = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    PGTDishViewModel *viewModel = [[PGTDishViewModel alloc] init];
    viewModel.dish = dish;
    cell.viewModel = viewModel;
    
    [cell setTintColor:[UIColor colorWithHexString:COLOR_DARK_BROWN]];
    
    PGTCustomColoredAccesory *accessory = [PGTCustomColoredAccesory accessoryWithColor:[UIColor colorWithHexString:COLOR_DARK_BROWN]];
    accessory.highlightedColor = [UIColor blackColor];
    cell.accessoryView = accessory;
    
    return cell;
    
}


- (void)notificationWhenMOCChanges{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeModel) name:UIDocumentStateChangedNotification object:self.managedDocument];
}

- (void)removeNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initializeModel {
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:dishEntityName];
    fetch.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:dishPropertyName ascending:YES]];
    
    NSFetchedResultsController *results = [[NSFetchedResultsController alloc] initWithFetchRequest:fetch managedObjectContext:self.managedDocument.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController = results;
    
    [self.tableView setBounces:YES];
    if(![[self.fetchedResultsController fetchedObjects] count]){
        [self.view bringSubviewToFront:self.feedbackView];
        [self.feedbackView show:YES animated:YES];
        [self.tableView setBounces:NO];
    }
    
    else{
        [self.feedbackView show:NO animated:NO];
    }

    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    Dish *dish = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (editingStyle) {
        case UITableViewCellEditingStyleNone:
            break;
        case UITableViewCellEditingStyleDelete:
            [self.managedDocument.managedObjectContext.undoManager beginUndoGrouping];
            [self.managedDocument.managedObjectContext deleteObject:dish];
            [self.managedDocument.managedObjectContext.undoManager endUndoGrouping];
            [self initializeModel];
            break;
        case UITableViewCellEditingStyleInsert:
            break;
            
        default:
            break;
    }
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:createSegueName]) {
        PGTEditDishTVC *dishVC = (PGTEditDishTVC *)[segue.destinationViewController topViewController];
        [self prepareDishEditVC:dishVC withDish:nil];
    } else if ([[segue identifier] isEqualToString:editSegueName]) {
        
        PGTEditDishTVC *dishVC = (PGTEditDishTVC *)[segue.destinationViewController topViewController];
        Dish *dish = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        [self prepareDishEditVC:dishVC withDish:dish];
    }
    
}


- (void)prepareDishEditVC:(PGTEditDishTVC *)controller withDish:(Dish *)dish {
    [self.managedDocument.managedObjectContext.undoManager beginUndoGrouping];
    PGTDishViewModel *viewModel = [[PGTDishViewModel alloc] init];
    
    if(!dish){
        dish = [Dish createDishInMoc:self.managedDocument.managedObjectContext];
    }
    
    viewModel.dish = dish;

    viewModel.managedDocument = self.managedDocument;
    controller.delegate = self;
    controller.viewModel = viewModel;
    controller.managedDocument = self.managedDocument;
    
    
    
}

- (IBAction)downloadDishes:(id)sender {

    PGTImportedDishesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:importedDishesVCName];
    vc.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width);
    vc.delegate = self;
    [self presentSemiViewController:vc withOptions:nil completion:nil dismissBlock:^{
    }];

    
    
}

- (void)dismissVC:(PGTImportedDishesViewController *)controller withDishes:(NSArray *)dishes{
    [self dismissSemiModalView];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [self.managedDocument.managedObjectContext.undoManager beginUndoGrouping];
        
        for(NSDictionary *dict in dishes){
            [Dish dishInMOC:self.managedDocument.managedObjectContext withDictionary:dict];
        }
        [self.managedDocument.managedObjectContext.undoManager endUndoGrouping];
        
        [self initializeModel];
        
    });
    
    

}


#pragma mark - Delegates

- (void)dismissEditViewController:(PGTEditDishTVC *)editVC modifiedData:(BOOL)modifiedData{
    
    [self.managedDocument.managedObjectContext.undoManager endUndoGrouping];
    
    if(!modifiedData){
        [self.managedDocument.managedObjectContext.undoManager undo];
    }
    
    [editVC dismissViewControllerAnimated:YES completion:^{
        [self initializeModel];
        
    }];
    
    
}






@end
