//
//  PGTWeekTableViewController.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTWeekTableViewController.h"
#import "Day+Model.h"
#import "Dish+Model.h"
#import "PGTDishesAPIHelper.h"
#import "PGTDateManager.h"
#import "PGTDayTableViewCell.h"
#import "PGTDayTVC.h"
#import "PGTCreateWeekTableViewCell.h"
#import "PGTAlerts.h"
#import "PGTFeedbackView.h"
#import "MBProgressHUD.h"

NSString *const dayMomentCellName = @"dayMomentCell";
NSString *const weekCreatorCellName = @"weekCreatorCell";
NSString *const fullDaySegue = @"fullDaySegue";


NSString *const feedbackViewName = @"PGTFeedbackView";


@interface PGTWeekTableViewController () <UIAlertViewDelegate>

@property (nonatomic,copy) NSArray *weekMenu;
@property (nonatomic) BOOL needShowNewWeekCell;
@property (nonatomic) NSUInteger numberOfDaysInView;

@property (nonatomic,strong) UIAlertView *alertDelete;
@property (nonatomic,strong) UIAlertView *alertCreate;

@property (nonatomic,strong) PGTFeedbackView *feedbackView;

@end

@implementation PGTWeekTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (PGTFeedbackView *)feedbackView{
    if(!_feedbackView){
        _feedbackView = [[[NSBundle mainBundle] loadNibNamed:feedbackViewName owner:self options:nil] objectAtIndex:0];
        [self.feedbackView setupWithTip1:NSLocalizedString(@"tip1Text", @"feedBackTutorial")
                                 andTip2:NSLocalizedString(@"tip2Text", @"feedBackTutorial")];
//        [self.feedbackView show:NO animated:NO];
        self.feedbackView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-49);
        
    }
    
    return _feedbackView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self notificationWhenMOCChanges];
    [self notificationWhenDateChanges];
    self.numberOfDaysInView = 14;
    [self setupView];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupView];
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initializeModel];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeNotifications];
}


- (void)setupView{
    if(!self.feedbackView.superview){
        [self.tableView addSubview:self.feedbackView];

    }
    
}

- (void)createRandomMenu {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDate *firstDay = [PGTDateManager getZeroHourFromDate:[NSDate date]];
        NSUInteger numberOfDays = [self.weekMenu count] ? [self.weekMenu count] : 7;
        BOOL created = [PGTDishesAPIHelper createWeekMenuInMOC:self.managedDocument.managedObjectContext
                                                 withStartDate:firstDay
                                                  numberOfDays:numberOfDays];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if(created){
            [self initializeModel];
        }
        
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"titleNeedDishes", nil)
                                                            message:NSLocalizedString(@"descriptionNeedDishes", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"cancelButtonNeedDishes", nil)
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    });

    
    
}

- (void)addNewWeekMenu {
    NSDate *firstDay = [PGTDateManager getZeroHourFromDate:[NSDate date]];
    NSDate *nextWeekDay = [NSDate dateWithTimeInterval:[PGTDateManager secondsForDays:[self.weekMenu count]] sinceDate:firstDay];
    [PGTDishesAPIHelper createWeekMenuInMOC:self.managedDocument.managedObjectContext
                              withStartDate:nextWeekDay
                               numberOfDays:7];
    
    [self initializeModel];
}

#pragma mark - Table view data source

- (void)notificationWhenMOCChanges{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeModel) name:UIDocumentStateChangedNotification object:self.managedDocument];
}

- (void)notificationWhenDateChanges{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeModel) name:DATE_CHANGE_NOTIFICATION object:nil];
}


- (void)removeNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initializeModel {
    
    NSDate *firstDay = [PGTDateManager getZeroHourFromDate:[NSDate date]];
    NSUInteger lastIntervalDay = self.numberOfDaysInView - 1;
    
    self.weekMenu = [PGTDishesAPIHelper weekMenuInMOC:self.managedDocument.managedObjectContext
                                          ForWeekDate:firstDay
                                         numberOfDays:lastIntervalDay];
    
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
    
    [self setActualDate];
    [self setWeekSummary];
    

}

- (void)setWeekSummary {
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:WIDGET_GROUP_NAME];
    
    NSMutableArray *week = [[NSMutableArray alloc] init];

    for (Day *day in self.weekMenu) {
        
        PGTDayViewModel *viewModel = [[PGTDayViewModel alloc] init];
        viewModel.day = day;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        NSDate *date = viewModel.date;
        NSString *lunchName = [viewModel dayMomentNameWithDayMomentTypeIndex:0];
        NSString *dinnerName = [viewModel dayMomentNameWithDayMomentTypeIndex:1];
        NSString *lunch = [viewModel dishNameWithDayMomentType:lunchName];
        NSString *dinner = [viewModel dishNameWithDayMomentType:dinnerName];
        
        [dict setObject:date forKey:@"date"];
        if(lunch){
          [dict setObject:lunch forKey:@"lunch"];
        }
        if(dinner){
           [dict setObject:dinner forKey:@"dinner"];
        }
        
        [week addObject:dict];
        
    }
    
    [sharedDefaults setObject:week forKey:WIDGET_SUMMARY_KEY];
    [sharedDefaults synchronize];
}

- (void)setActualDate {
    NSDate *actualDate = [PGTDateManager getZeroHourFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults] setObject:actualDate forKey:SETTINGS_LAST_DATE];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    [self.tableView setBounces:YES];
    if(![self.weekMenu count]){
        [self.view bringSubviewToFront:self.feedbackView];
        [self.feedbackView show:YES animated:YES];
        [self.tableView setBounces:NO];
    }
    
    else{
        [self.feedbackView show:NO animated:NO];
    }
    
    if([self.weekMenu count] && [self.weekMenu count]<7){
        self.needShowNewWeekCell = YES;
        return ([self.weekMenu count] + 1);
    }
    return [self.weekMenu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == [self.weekMenu count]){
        PGTCreateWeekTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:weekCreatorCellName];
        return cell;
    }
    
    else{
        PGTDayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dayMomentCellName];
        
        PGTDayViewModel *viewModel = [[PGTDayViewModel alloc] init];
        Day *day = self.weekMenu[indexPath.row];
        viewModel.day = day;
        cell.emphasize = indexPath.row == 0 ? YES : NO;
        
        cell.viewModel = viewModel;
        
        return cell;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.needShowNewWeekCell && indexPath.row == [self.weekMenu count]){
        [self addNewWeekMenu];
    }
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:fullDaySegue]){
        PGTDayTVC *destination = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Day *day = self.weekMenu[indexPath.row];
        PGTDayViewModel *viewModel = [[PGTDayViewModel alloc] init];
        viewModel.day = day;
        
        destination.viewModel = viewModel;
        destination.managedDocument = self.managedDocument;
    }
}


#pragma mark - Actions

- (IBAction)chooseCreateMenu:(id)sender {
    if([self.weekMenu count]){
        self.alertCreate = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"titleReplaceMenu", @"")
                                                      message:NSLocalizedString(@"descriptionReplaceMenu", @"")
                                                     delegate:self
                                            cancelButtonTitle:NSLocalizedString(@"cancelButtonReplaceMenu", @"")
                                            otherButtonTitles:NSLocalizedString(@"confirmButtonReplaceMenu", @""), nil];
        
        [self.alertCreate show];
    }
    
    else{
        [self createRandomMenu];
    }

}
- (IBAction)deleteDays:(id)sender {
    
    if([self.weekMenu count]){
        
        self.alertDelete = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"titleDeleteMenu", @"")
                                                      message:NSLocalizedString(@"descriptionDeleteMenu", @"")
                                                     delegate:self
                                            cancelButtonTitle:NSLocalizedString(@"cancelButtonDeleteMenu", @"")
                                            otherButtonTitles:NSLocalizedString(@"confirmButtonDeleteMenu", @""), nil];
        [self.alertDelete show];
        
    }
    
}
- (IBAction)addNewWeekPushed:(id)sender {
    [self addNewWeekMenu];
}

- (void)deleteWeekMenu {
    
    for (Day *day in self.weekMenu) {
        [self.managedDocument.managedObjectContext deleteObject:day];
    }
    
    [self initializeModel];
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView == self.alertDelete){
        if(buttonIndex == 1){
            [self deleteWeekMenu];
        }
    }
    
    else if (alertView == self.alertCreate){
        if(buttonIndex == 1){
            [self createRandomMenu];
        }
    }
    
}

@end
