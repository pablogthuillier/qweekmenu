//
//  PGTEditDishTVC.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 17/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTEditDishTVC.h"
#import "Dish+Model.h"
#import "UIViewController+KNSemiModal.h"
#import "PGTIngredientsViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "PGTDateManager.h"
#import "PGTImageManager.h"
#import "MBProgressHUD.h"
#import "PGTEditDishTableViewCell.h"

NSString *const ingredientsCellName = @"IngredientCell";
NSString *const ingredientsVCName = @"PGTIngredientsViewController";

int const defaultMerging = 60;
int const heighSection = 44;

@interface PGTEditDishTVC () <IngredientsListDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *dishNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *dishImageView;

@property (nonatomic,strong) UIImagePickerController *imagePicker;

@property (nonatomic,strong) UIImage *dishPhoto;
@property (nonatomic,copy) NSString *imgURL;
@property (weak, nonatomic) IBOutlet UIImageView *photoIcon;

@property (nonatomic,strong) PGTImageManager *imgManager;


@end

@implementation PGTEditDishTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    
}


- (void)setupView {
    
    self.dishNameTextField.text = NSLocalizedString(@"dishName", nil) ;
    self.dishNameTextField.clearsOnBeginEditing = YES;
    
    if(self.viewModel.dishName && ![self.viewModel.dishName isEqualToString:@""]){
        self.dishNameTextField.text = self.viewModel.dishName;
        self.dishNameTextField.clearsOnBeginEditing = NO;
    }

    
    self.dishNameTextField.font = CUSTOM_FONT_NORMAL_LABEL;
    self.dishImageView.image = self.viewModel.dishImage ? self.viewModel.dishImage : [self.viewModel defaultDishImage];
    if(self.viewModel.dishImage) {
        self.photoIcon.alpha = 0;
    }
    [self.dishImageView setClipsToBounds:YES];
    [self.dishImageView setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCamera:)];
    [self.dishImageView addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tapDismissKeyboard = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapDismissKeyboard];
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imgManager = [[PGTImageManager alloc] init];
}


#pragma mark - Table Data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTEditDishTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ingredientsCellName];
    IngredientDish *ingredientDish = [[self.viewModel dishIngredients] objectAtIndex:indexPath.row];
    PGTIngredientDishViewModel *viewModel = [[PGTIngredientDishViewModel alloc] init];
    viewModel.ingredientDish = ingredientDish;
    cell.viewModel = viewModel;
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.dishIngredientsCount;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, heighSection)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, self.view.bounds.size.width - defaultMerging, 24)];
    label.text = NSLocalizedString(@"ingredientsTitle", nil);
    label.font = CUSTOM_FONT_NORMAL_LABEL;
    
    view.backgroundColor = [UIColor colorWithHexString:COLOR_DARK_BROWN];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"+" forState:UIControlStateNormal];
    [button.titleLabel setFont:CUSTOM_FONT_BIG_LABEL];
    button.frame = CGRectMake(self.view.bounds.size.width - defaultMerging, 10, 34, 24);
    
    [button addTarget:self action:@selector(openIngredientsModal) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:label];
    [view addSubview:button];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return heighSection;
}


#pragma mark - Actions
- (IBAction)cancelPushed:(id)sender {
    [self.delegate dismissEditViewController:self modifiedData:NO];
}


- (IBAction)donePushed:(id)sender {
    [self assignDataToDish];
    [self.delegate dismissEditViewController:self modifiedData:YES];
}

- (void)assignDataToDish{
    [self.viewModel saveDishDataWithName:self.dishNameTextField.text andImageURL:self.imgURL];

}

- (void)openIngredientsModal {
    [self dismissKeyboard];
    PGTIngredientsViewController *ingredientsVC = [self.storyboard instantiateViewControllerWithIdentifier:ingredientsVCName];
    ingredientsVC.ingredients = self.viewModel.totalIngredients;
    ingredientsVC.selectedIngredients = [[self.viewModel dishIngredientsArray] mutableCopy];
    ingredientsVC.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width);
    ingredientsVC.delegate = self;
    [self presentSemiViewController:ingredientsVC withOptions:nil completion:nil dismissBlock:^{
    }];
    
}



#pragma mark - Ingredients List Delegate

- (void)dismissVC:(PGTIngredientsViewController *)controller WithArray:(NSArray *)ingredients{
    
    
    if(ingredients){
        [self.viewModel saveDishIngredients:ingredients];
    }

    [self dismissSemiModalView];
    [self.tableView reloadData];

}

-(void)dismissKeyboard {
    [self.dishNameTextField resignFirstResponder];
}

#pragma mark - Camera function

- (void) openCamera:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        self.imagePicker.delegate = self;
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        self.imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        self.imagePicker.allowsEditing = NO;
        [self presentViewController:self.imagePicker
                           animated:YES completion:nil];
    }
}

- (void) useCamera:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
}

- (void) useCameraRoll:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.imagePicker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(useCamera:)];
        viewController.navigationItem.leftBarButtonItems = [NSArray arrayWithObject:button];
    } else {
        UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"libraryName", nil) style:UIBarButtonItemStylePlain target:self action:@selector(useCameraRoll:)];
        viewController.navigationItem.leftBarButtonItems = [NSArray arrayWithObject:button];
        viewController.navigationItem.title = NSLocalizedString(@"takePhotoName", nil);
        viewController.navigationController.navigationBarHidden = NO;
    }
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [MBProgressHUD showHUDAddedTo:self.dishImageView animated:YES];
    int timestamp = [[NSDate date] timeIntervalSince1970];
    NSString *fileName = [NSString stringWithFormat:@"%d.png",timestamp];
    
    [self.imgManager saveMediaWithInfo:info fileName:fileName completionBlock:^(UIImage * imgToSave) {
        self.dishPhoto = imgToSave;
        [self.dishImageView setImage:imgToSave];
        self.imgURL = fileName;
        [MBProgressHUD hideAllHUDsForView:self.dishImageView animated:YES];

    }];

}



#pragma mark - Text Field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}



@end
