//
//  PGTSettingsTableViewController.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTSettingsTableViewController : UITableViewController

@end
