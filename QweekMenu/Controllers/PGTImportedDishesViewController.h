//
//  PGTImportedDishesViewController.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 03/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PGTImportedDishesViewController;
extern NSString *const importedDishesVCName;

@protocol PGTImportedDishDelegate <NSObject>

- (void)dismissVC:(PGTImportedDishesViewController *)controller withDishes:(NSArray *)dishes;

@end


@interface PGTImportedDishesViewController : UIViewController

@property (nonatomic,weak) id<PGTImportedDishDelegate> delegate;

@end
