//
//  PGTDishesTableViewController.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PGTEditDishTVC.h"

@interface PGTDishesTableViewController : CoreDataTableViewController

@property (nonatomic,strong) UIManagedDocument *managedDocument;

@end
