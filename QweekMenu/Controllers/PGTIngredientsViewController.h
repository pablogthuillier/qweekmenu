//
//  PGTIngredientsViewController.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PGTIngredientsViewController;

@protocol IngredientsListDelegate <NSObject>

- (void)dismissVC:(PGTIngredientsViewController *)controller WithArray:(NSArray *)ingredients;

@end

@interface PGTIngredientsViewController : UIViewController

@property (nonatomic,weak) id<IngredientsListDelegate> delegate;

@property (nonatomic,copy) NSArray *ingredients;
@property (nonatomic,strong) NSMutableArray *selectedIngredients;

@end
