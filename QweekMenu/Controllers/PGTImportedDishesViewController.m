//
//  PGTImportedDishesViewController.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 03/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTImportedDishesViewController.h"
#import "PGTImportedDishTableViewCell.h"
#import "Dish+Model.h"

NSString *const importedDishCellName = @"importedDishCell";
NSString *const importedDishesVCName = @"PGTImportedDishesViewController";

@interface PGTImportedDishesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,copy) NSArray *dishes;
@property (nonatomic,strong) NSMutableArray *selectedDishes;

@end

@implementation PGTImportedDishesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData {
    NSString *pathFile = [[NSBundle mainBundle]pathForResource:@"InitialData" ofType:@"plist"];
    self.dishes = [NSMutableArray arrayWithContentsOfFile:pathFile];
    self.selectedDishes = [[NSMutableArray alloc] init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dishes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PGTImportedDishTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:importedDishCellName];
    [self setSelectedStyleForCell:cell selected:NO];
    NSDictionary *dictDish = [self.dishes objectAtIndex:indexPath.row];
    
    NSString *imageName = [dictDish valueForKey:dishPropertyPhoto];
    
    if([self.selectedDishes containsObject:dictDish]){
        [self setSelectedStyleForCell:cell selected:YES];
    }
    
    cell.dishImageView.image = [UIImage imageNamed:imageName];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *name = [dictDish valueForKey:dishPropertyName];
    if(![language isEqualToString:DEFAULT_LANGUAGE_CODE]){
        for (NSDictionary *localDish in [dictDish valueForKey:dishPropertyLocalizedNames]) {
            if([[localDish objectForKey:localizedDishPropertyLanguage] isEqualToString:language]){
                name = [localDish objectForKey:localizedDishPropertyName];
            }
        }
    }

    
    cell.dishName.text = name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTImportedDishTableViewCell *cell = (PGTImportedDishTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSDictionary *dictDish = [self.dishes objectAtIndex:indexPath.row];
    
    if(![self.selectedDishes containsObject:dictDish]){
        [self setSelectedStyleForCell:cell selected:YES];
        [self.selectedDishes addObject:dictDish];
        
    }
    
    else{
        [self setSelectedStyleForCell:cell selected:NO];
        [self.selectedDishes removeObject:dictDish];
    }

}

- (void)setSelectedStyleForCell:(UITableViewCell *)cell selected:(BOOL)selected {
    
    if(selected){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else{
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
}

- (IBAction)donePushed:(UIBarButtonItem *)sender {
    [self.delegate dismissVC:self withDishes:self.selectedDishes];
}

@end
