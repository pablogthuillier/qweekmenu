//
//  PGTWeekTableViewController.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"

@interface PGTWeekTableViewController : UITableViewController

@property (nonatomic,strong) UIManagedDocument *managedDocument;

@end
