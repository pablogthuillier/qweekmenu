//
//  PGTDayTVC.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "PGTDayViewModel.h"

@interface PGTDayTVC : UITableViewController

@property (nonatomic,strong) UIManagedDocument *managedDocument;

@property (nonatomic,strong) PGTDayViewModel *viewModel;

@end
