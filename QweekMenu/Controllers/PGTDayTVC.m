//
//  PGTDayTVC.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDayTVC.h"
#import "DayMoment+Model.h"
#import "PGTDishDayTableViewCell.h"
#import "PGTDishViewModel.h"
#import "PGTEditDishTVC.h"
#import "PGTChooseDishViewController.h"
#import "UIViewController+KNSemiModal.h"
#import "PGTDishesCollectionViewModel.h"

NSString *const dishDayCellName = @"dishCell";
NSString *const editDayDishSegue = @"editDayDishSegue";
NSString *const editDishTVC =@"PGTEditDishTVC";


int const sectionHeight = 44;

@interface PGTDayTVC () <PGTDishDayCellDelegate, PGTEditDishTVCDelegate, PGTChooseDishDelegate>

@end

@implementation PGTDayTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.viewModel numberOfDayMoments];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.viewModel dishWithDayMomentTypeIndex:section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, sectionHeight)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.bounds.size.width, 24)];
    label.text = [NSLocalizedString([self.viewModel dayMomentNameWithDayMomentTypeIndex:section], @"dayMomentType") capitalizedString];
    label.font = CUSTOM_FONT_NORMAL_LABEL;
    
    view.backgroundColor = [UIColor colorWithHexString:COLOR_DARK_BROWN];
    
    [view addSubview:label];
    
    return view;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return sectionHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTDishDayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dishDayCellName];
    PGTDishViewModel *viewModel = [[PGTDishViewModel alloc] init];
    Dish *dish = [[self.viewModel dishWithDayMomentTypeIndex:indexPath.section] objectAtIndex:indexPath.row];
    viewModel.dish = dish;
    viewModel.managedDocument = self.managedDocument;
    cell.viewModel = viewModel;
    cell.delegate = self;
    
    return cell;
    
    

}


// ios8 feature

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{

    
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewRowAction *changeAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Change" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        PGTDishDayTableViewCell *cell = (PGTDishDayTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        PGTDishesCollectionViewModel *viewModel = [[PGTDishesCollectionViewModel alloc] init];
        viewModel.managedDocument = self.managedDocument;
        [self openDishSelectorWithViewModel:viewModel
                               DishSelected:cell.viewModel.dish
                            inDayMomentName:[self.viewModel dayMomentNameWithDayMomentTypeIndex:indexPath.section]];
        
    }];
    changeAction.backgroundColor = [UIColor colorWithHexString:COLOR_BLUE_LIGHT];
    
    return @[changeAction];
}


#pragma mark - Delegates

- (void)openDishEditorWithViewModel:(PGTDishViewModel *)viewModel{
    [self performSegueWithIdentifier:editDayDishSegue sender:viewModel];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:editDayDishSegue]){
        [self.managedDocument.managedObjectContext setUndoManager:[[NSUndoManager alloc] init]];
        [self.managedDocument.managedObjectContext.undoManager beginUndoGrouping];
        PGTEditDishTVC *destination = (PGTEditDishTVC *)[[segue destinationViewController] topViewController];
        destination.viewModel = (PGTDishViewModel *)sender;
        destination.delegate = self;
    }
    
}

- (void)dismissEditViewController:(PGTEditDishTVC *)editVC modifiedData:(BOOL)modifiedData{
    
    [self.managedDocument.managedObjectContext.undoManager endUndoGrouping];
    
    if(!modifiedData){
        [self.managedDocument.undoManager undo];
    }
    
    [editVC dismissViewControllerAnimated:YES completion:^{
        [self.tableView reloadData];
        
    }];
}

- (void)openDishSelectorWithViewModel:(PGTDishesCollectionViewModel *)viewModel
                                                                     DishSelected:(Dish *)dish
                                                                  inDayMomentName:(NSString *)dayMomentName{
    PGTChooseDishViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:chooseDishVCName];
    vc.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width);
    vc.delegate = self;
    vc.viewModel = viewModel;
    vc.selectedDish = dish;
    vc.dayMomentName = dayMomentName;
    [self presentSemiViewController:vc withOptions:nil completion:nil dismissBlock:^{
    }];
}


- (void)dismissVC:(PGTChooseDishViewController *)controller withDish:(Dish *)dish inDayMomentName:(NSString *)dayMomentName{
    [self dismissSemiModalView];
    [self.managedDocument.managedObjectContext.undoManager beginUndoGrouping];
    [DayMoment createDayMomentInMOC:self.managedDocument.managedObjectContext
                           withDish:dish
                               date:self.viewModel.date
                            andType:dayMomentName
                            replace:YES];
    [self changeWeekSumary];
    [self.managedDocument.managedObjectContext.undoManager endUndoGrouping];
    
    [self.tableView reloadData];

}

- (void)changeWeekSumary{
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:WIDGET_GROUP_NAME];
    
    NSMutableArray *week = [[sharedDefaults objectForKey:WIDGET_SUMMARY_KEY] mutableCopy];
    __block NSMutableDictionary *dictToChange = [[NSMutableDictionary alloc] init];
    __block NSInteger idxToChange = -1;
    
    [week enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = (NSDictionary *)obj;
        dictToChange = [dict mutableCopy];
        NSDate *date = [dict objectForKey:@"date"];
        if([date isEqual:self.viewModel.date]){
            NSString *lunchName = [self.viewModel dayMomentNameWithDayMomentTypeIndex:0];
            NSString *dinnerName = [self.viewModel dayMomentNameWithDayMomentTypeIndex:1];
            NSString *lunch = [self.viewModel dishNameWithDayMomentType:lunchName];
            NSString *dinner = [self.viewModel dishNameWithDayMomentType:dinnerName];
            
            [dictToChange setObject:lunch forKey:@"lunch"];
            [dictToChange setObject:dinner forKey:@"dinner"];
            idxToChange = idx;
        }

    }];
    
    if (idxToChange >=0) {
        [week replaceObjectAtIndex:idxToChange withObject:dictToChange];
    }
    
    [sharedDefaults setObject:week forKey:WIDGET_SUMMARY_KEY];
    [sharedDefaults synchronize];
}

@end
