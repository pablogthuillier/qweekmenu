//
//  PGTIngredientsViewController.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTIngredientsViewController.h"
#import "Ingredient+Model.h"
#import "PGTIngredientTableViewCell.h"
#import "PGTIngredientViewModel.h"
#import "IngredientDish.h"

NSString *const tableViewCellName = @"IngredientCell";


@interface PGTIngredientsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *imgIngredient;
@property (weak, nonatomic) IBOutlet UILabel *lblIngredient;

@end

@implementation PGTIngredientsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSMutableArray *)selectedIngredients{
    if(!_selectedIngredients){
        _selectedIngredients = [[NSMutableArray alloc] init];
    }
    
    return _selectedIngredients;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.ingredients count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTIngredientTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellName];
    [self setSelectedStyleForCell:cell selected:NO];
    Ingredient *ingredient = self.ingredients[indexPath.row];
    PGTIngredientViewModel *viewModel = [[PGTIngredientViewModel alloc] init];
    viewModel.ingredient = ingredient;
    cell.viewModel = viewModel;
    if([self.selectedIngredients containsObject:ingredient]){
        [self setSelectedStyleForCell:cell selected:YES];
    }
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTIngredientTableViewCell *cell = (PGTIngredientTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    IngredientDish *ingredientDish = self.ingredients[indexPath.row];
    
    if(![self.selectedIngredients containsObject:ingredientDish]){
        [self setSelectedStyleForCell:cell selected:YES];
        [self.selectedIngredients addObject:ingredientDish];
        
    }
    
    else{
        [self setSelectedStyleForCell:cell selected:NO];
        [self.selectedIngredients removeObject:ingredientDish];
    }
    
}


- (IBAction)donePushed:(id)sender {
    [self.delegate dismissVC:self WithArray:self.selectedIngredients];
    
    
}


- (void)setSelectedStyleForCell:(UITableViewCell *)cell selected:(BOOL)selected {
    
    if(selected){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else{
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
}

- (void)setupView {
    self.view.frame = CGRectMake(0, 0, 320, 100);
}


@end
