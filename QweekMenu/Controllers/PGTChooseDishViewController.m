//
//  PGTChooseDishViewController.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTChooseDishViewController.h"
#import "PGTChooseDishTableViewCell.h"
#import "PGTDishViewModel.h"

NSString *const chooseDishVCName = @"PGTChooseDishViewController";
NSString *const cellName = @"chooseDishCell";

@interface PGTChooseDishViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) NSArray *dishes;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PGTChooseDishViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
}

- (void)setViewModel:(PGTDishesCollectionViewModel *)viewModel{
    _viewModel = viewModel;
    [self loadData];
}

- (void)loadData {
    self.dishes = [self.viewModel allDishes];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dishes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTChooseDishTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName forIndexPath:indexPath];
    
    Dish *dish = self.dishes[indexPath.row];
    
    PGTDishViewModel *viewModel = [[PGTDishViewModel alloc] init];
    viewModel.dish = dish;
    cell.vieModel = viewModel;
    
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    if([self.selectedDish isEqual:dish]){
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    
   
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTChooseDishTableViewCell *cell = (PGTChooseDishTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    Dish *dictDish = [self.dishes objectAtIndex:indexPath.row];
    
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    self.selectedDish = dictDish;
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    PGTChooseDishTableViewCell *cell = (PGTChooseDishTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
}



#pragma mark - Actions

- (IBAction)donePushed:(UIBarButtonItem *)sender {
    [self.delegate dismissVC:self withDish:self.selectedDish inDayMomentName:self.dayMomentName];
}


@end
