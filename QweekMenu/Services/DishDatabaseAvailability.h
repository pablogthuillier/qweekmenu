//
//  DishDatabaseAvailability.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 15/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#ifndef QweekMenu_DishDatabaseAvailability_h
#define QweekMenu_DishDatabaseAvailability_h

#define DishDatabaseAvailabilityNotification @"DishDatabaseAvailabilityNotification"
#define DishDatabaseAvailabilityDocument @"Document"



#endif
