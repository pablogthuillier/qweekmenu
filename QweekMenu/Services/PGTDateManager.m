//
//  PGTDateManager.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 18/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDateManager.h"

NSUInteger const defaultFirstDayOfWeek = 2;
NSUInteger const dayInSeconds = 60*60*24;

@implementation PGTDateManager

+ (NSDate *)getFirstDayOfWeekWithDate:(NSDate *)date withFirstDayOfTheWeek:(NSUInteger)firstDayOfWeek{
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    
    [gregorian setFirstWeekday:firstDayOfWeek];
    
    // Get the weekday component of the current date
    NSDateComponents *weekdayComponents = [gregorian components:NSCalendarUnitWeekday fromDate:date];
    /*
     Create a date components to represent the number of days to subtract
     from the current date.
     The weekday value for Sunday in the Gregorian calendar is 1, so
     subtract 1 from the number
     of days to subtract from the date in question.  (If today's Sunday,
     subtract 0 days.)
     */
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    /* Substract [gregorian firstWeekday] to handle first day of the week being something else than Sunday */
    [componentsToSubtract setDay: - ([weekdayComponents weekday] - [gregorian firstWeekday])];
    NSDate *beginningOfWeek = [gregorian dateByAddingComponents:componentsToSubtract toDate:date options:0];
    
    /*
     Optional step:
     beginningOfWeek now has the same hour, minute, and second as the
     original date (today).
     To normalize to midnight, extract the year, month, and day components
     and create a new date from those components.
     */

    beginningOfWeek = [self getZeroHourFromDate:beginningOfWeek];
    return beginningOfWeek;
}

+ (NSDate *)getZeroHourFromDate:(NSDate *)date{
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [gregorian components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                                fromDate: date];
    NSDate *dateZero = [gregorian dateFromComponents: components];
    return dateZero;
}


+ (NSString *)localStringFromDate:(NSDate *)date {
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
}

+ (NSString *)shortStringFromDate:(NSDate *)date {
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
}

+ (NSString *)dateWithDayAndMonth:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd"];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:SPANISH_LANGUAGE_CODE]){
        [dateFormatter setDateFormat:@"dd/MM"];
    }
    
    return [dateFormatter stringFromDate:date];
}

+ (NSUInteger)secondsForDays:(NSUInteger)numberOfDays{
    return dayInSeconds * numberOfDays;
}

+ (NSUInteger)dayOfTheWeekFromDate:(NSDate *)date {
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    
    NSDateComponents *weekdayComponents =[gregorian components:NSCalendarUnitWeekday fromDate:date];
    
    return [weekdayComponents weekday];
}

+ (NSString *)firstCharOfDay:(NSUInteger)numberDay {
    
    NSString *character;
    
    switch (numberDay) {
        case 0:
            character = @"";
            break;
        case 1:
            character = NSLocalizedString(@"sundayInitial", nil);
            break;
        case 2:
            character = NSLocalizedString(@"mondayInitial", nil);
            break;
        case 3:
            character = NSLocalizedString(@"tuesdayInitial", nil);
            break;
        case 4:
            character = NSLocalizedString(@"wednesdayInitial", nil);
            break;
        case 5:
            character = NSLocalizedString(@"thursdayInitial", nil);
            break;
        case 6:
            character = NSLocalizedString(@"fridayInitial", nil);
            break;
        case 7:
            character = NSLocalizedString(@"satudarInitial", nil);
            break;
        default:
            break;
    }
    
    return character;
}

@end
