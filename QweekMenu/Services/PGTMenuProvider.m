//
//  PGTMenuProvider.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 15/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTMenuProvider.h"
#import "Dish+Model.h"
#import "Day+Model.h"
#import "DayMoment+Model.h"
#import "PGTDateManager.h"
#import "IngredientType+Model.h"
#import "IngredientDish+Model.h"

NSInteger const minProximityBetweenDishes = 4;
NSInteger const numberOfMomentsInADay = 2;

NSString *const starterName = @"Starter";
NSString *const legumeName = @"legumes";
NSString *const meatName = @"meat";
NSString *const fishName = @"fish";
NSString *const cerealsName = @"cereals";
NSString *const fruitsName = @"fruits";

@implementation PGTMenuProvider

+ (BOOL)menuForDays:(NSUInteger)days inMOC:(NSManagedObjectContext *)moc andStartDay:(NSDate *)firstDay{
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:dishEntityName];
    NSArray *dishes = [moc executeFetchRequest:fetch error:nil];
    
    if(![dishes count]){
        return NO;
    }
    
    if([dishes count]<5){
        return NO;
    }
    
    NSMutableArray *ocurrences = [[NSMutableArray alloc] init];
    NSMutableArray *latestDishes = [[NSMutableArray alloc] init];
    
    int meatMax = [[[NSUserDefaults standardUserDefaults] objectForKey:SETTINGS_MEAT_MAX_NAME] intValue];
    int cerealsMax = [[[NSUserDefaults standardUserDefaults] objectForKey:SETTINGS_CEREALS_MAX_NAME] intValue];
    
    int meatCount = 0;
    int cerealsCount = 0;
    BOOL maxMeatReached = NO;
    BOOL maxCerealsReached = NO;
    
    int contProximity = 0;
    
    for (int i=0; i<days; i++) {
        
        if(i == 7){
            meatCount = 0;
            cerealsCount = 0;
            maxMeatReached = NO;
            maxCerealsReached = NO;
        }
        
        NSDate *date = [NSDate dateWithTimeInterval:(dayInSeconds*i) sinceDate:firstDay];
        for(int d=0; d < numberOfMomentsInADay; d++){
            NSString *type = d%2 == 0 ? lunchName : dinnerName;
            Dish *dish = [self getRandomDishWithDishes:dishes andOcurrences:ocurrences andLatestDishes:latestDishes starterDish:NO];
            int breakpointRetry = 0;
            Dish *lastChance = dish;
            while (![PGTMenuProvider dishIsValid:dish forType:type] ||
                   (maxMeatReached && [self dish:dish hasIngredientType:meatName]) ||
                   (maxCerealsReached && [self dish:dish hasIngredientType:cerealsName]) ) {
                dish = [self getRandomDishWithDishes:dishes andOcurrences:ocurrences andLatestDishes:latestDishes starterDish:NO];
                if([self dishIsValid:dish forType:type]){
                    lastChance = dish;
                }
                breakpointRetry++;
                if(breakpointRetry > 40){
                    dish = lastChance;
                    break;
                }

            }
            
            if(meatMax < 7){
                meatCount += [self dish:dish hasIngredientType:meatName] ? 1 : 0;
                maxMeatReached = meatCount >= meatMax ? YES : NO;
            }
            
            if(cerealsMax < 7){
                cerealsCount += [self dish:dish hasIngredientType:cerealsName] ? 1 : 0;
                maxCerealsReached = cerealsCount >= cerealsMax ? YES : NO;
            }
    
            
            
            
            
            [DayMoment createDayMomentInMOC:moc withDish:dish date:date andType:type replace:YES];
            
            if(contProximity > minProximityBetweenDishes){
                if([dishes count]<14){
                    [ocurrences removeObjectAtIndex:0];
                }
                
            }

            [ocurrences addObject:dish];
            [latestDishes addObject:dish];
            contProximity++;
        }
        

    }
    
    
    return YES;
}

+ (Dish *)getRandomDishWithDishes:(NSArray *)dishes andOcurrences:(NSArray *)ocurrences andLatestDishes:(NSArray *)latest starterDish:(BOOL)starter{
    NSUInteger myRandom = [self getRandomIndexWithTotal:[dishes count]];
    Dish *dish = [dishes objectAtIndex:myRandom];
    int exitPoint = 0;

    Dish *dishOcurrence = [ocurrences lastObject];
    BOOL canGetIt = NO;
    while (!canGetIt) {
        
        exitPoint++;
        if(exitPoint>30){
            break;
        }
        
        
        if([ocurrences containsObject:dish] ||
           (!starter && [dish.type.name isEqualToString:starterName]) ||
           [self dish:dishOcurrence hasRepeatedIngredientsWithDish:dish] ||
           [dish.ingredients count] == 0
           ){
            myRandom = [self getRandomIndexWithTotal:[dishes count]];
            dish = [dishes objectAtIndex:myRandom];
        }
        
        else{
            canGetIt = YES;
        }

    }

    
    return dish;
}

+ (NSUInteger)getRandomIndexWithTotal:(NSUInteger)total{
    return random() % total;
}


+ (BOOL)dish:(Dish *)dish1 hasRepeatedIngredientsWithDish:(Dish *)dish2 {
    
    BOOL haveRepeatedIngredients = NO;
    
    NSMutableSet *set1 = [[NSMutableSet alloc] init];
    for (IngredientDish *ingredientDish in dish1.ingredients) {
        [set1 addObject:ingredientDish.ingredient.type.name];
    }
    
    [set1 intersectSet:[self ingredientsThatCanNotBeRepeated]];
    if([set1 count]){
        NSMutableSet *set2 = [[NSMutableSet alloc] init];
        for (IngredientDish *ingredientDish in dish2.ingredients) {
            [set2 addObject:ingredientDish.ingredient.type.name];
        }
        [set2 intersectSet:set1];
        if([set2 count]){
            haveRepeatedIngredients = YES;
        }
    }
    
    
    return haveRepeatedIngredients;
}


+ (NSSet *)ingredientsThatCanBeRepeated {
    NSArray *repetitions = @[@"Olive oil", @"Sunflower oil", @"Tomato", @"Cheese", @"Salt", @"Onion", @"Garlic"];
    NSSet *set = [NSSet setWithArray:repetitions];
    return set;
}

+ (NSSet *)legumesToNotAvoid {
    NSArray *repetitions = @[@"Peas",@"Green beans"];
    NSSet *set = [NSSet setWithArray:repetitions];
    return set;
}

+ (NSSet *)ingredientsThatCanNotBeRepeated {
    
    NSArray *repetitions = @[meatName,
                             cerealsName,
                             fishName,
                             legumeName];
    
    NSSet *set = [NSSet setWithArray:repetitions];
    return set;
}

+ (BOOL)dishIsValid:(Dish *)dish forType:(NSString *)type{
    
    int avoidLegumesAtNight = [[[NSUserDefaults standardUserDefaults] objectForKey:SETTINGS_LEGUMES_NAME] intValue];
    NSSet *legumesAllowed = [self legumesToNotAvoid];
    
    if(avoidLegumesAtNight){
        for (IngredientDish *ingredientDish in [dish.ingredients allObjects]) {
            
            if([type isEqualToString:dinnerName]
               && [ingredientDish.ingredient.type.name isEqualToString:legumeName]
               && ![legumesAllowed containsObject:ingredientDish.ingredient.name]){
                
                return NO;
            }
        }
    }
    
    
    return YES;
}


+ (BOOL)dish:(Dish *)dish hasIngredientType:(NSString *)type{
    BOOL result = NO;
    NSMutableArray *types = [[NSMutableArray alloc] initWithObjects:type, nil];
    if([type isEqualToString:meatName]){
        [types addObject:fishName];
    }
    for (IngredientDish *ingredientDish in [dish.ingredients allObjects]) {
        if([types containsObject:ingredientDish.ingredient.type.name]){
            result = YES;
            
            
            return YES;
        }
    }
    return result;
    
}

@end
