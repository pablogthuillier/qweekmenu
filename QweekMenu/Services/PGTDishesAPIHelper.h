//
//  PGTDishesAPIHelper.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Dish;

extern NSString *const lunchName;
extern NSString *const dinnerName;

@interface PGTDishesAPIHelper : NSObject

+ (NSArray *)weekMenuInMOC:(NSManagedObjectContext *)moc ForWeekDate:(NSDate *)firstDay numberOfDays:(NSUInteger)numberOfDays;
+ (BOOL)createWeekMenuInMOC:(NSManagedObjectContext *)moc withStartDate:(NSDate *)firstDay numberOfDays:(NSUInteger)numberDays;

@end
