//
//  PGTMenuProvider.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 15/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Dish;

@interface PGTMenuProvider : NSObject

+ (BOOL)menuForDays:(NSUInteger)days inMOC:(NSManagedObjectContext *)moc andStartDay:(NSDate *)firstDay;

//for testing
+ (BOOL)dish:(Dish *)dish1 hasRepeatedIngredientsWithDish:(Dish *)dish2;
+ (Dish *)getRandomDishWithDishes:(NSArray *)dishes
                    andOcurrences:(NSArray *)ocurrences
                  andLatestDishes:(NSArray *)latest
                      starterDish:(BOOL)starter;

@end
