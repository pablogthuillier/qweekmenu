//
//  PGTDateManager.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 18/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSUInteger const defaultFirstDayOfWeek;
extern NSUInteger const dayInSeconds;

@interface PGTDateManager : NSObject

+ (NSDate *)getFirstDayOfWeekWithDate:(NSDate *)date withFirstDayOfTheWeek:(NSUInteger)firstDayOfWeek;
+ (NSDate *)getZeroHourFromDate:(NSDate *)date;
+ (NSString *)localStringFromDate:(NSDate *)date;
+ (NSString *)shortStringFromDate:(NSDate *)date;
+ (NSUInteger)secondsForDays:(NSUInteger)numberOfDays;
+ (NSString *)dateWithDayAndMonth:(NSDate *)date;
+ (NSUInteger)dayOfTheWeekFromDate:(NSDate *)date;
+ (NSString *)firstCharOfDay:(NSUInteger)numberDay;

@end
