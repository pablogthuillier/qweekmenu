//
//  PGTImageManager.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 28/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface PGTImageManager : NSObject

-(void)saveMediaWithInfo:(NSDictionary *)info fileName:(NSString *)fileName completionBlock:(void(^)(UIImage *))completion;

@end
