//
//  PGTDishesAPIHelper.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDishesAPIHelper.h"
#import "Dish+Model.h"
#import "DayMoment+Model.h"
#import "PGTDateManager.h"
#import "PGTMenuProvider.h"
#import "Day+Model.h"


int const daysInAWeek = 7;

@implementation PGTDishesAPIHelper


+ (NSArray *)weekMenuInMOC:(NSManagedObjectContext *)moc ForWeekDate:(NSDate *)firstDay numberOfDays:(NSUInteger)numberOfDays{
    
    NSDate *lastDate = [NSDate dateWithTimeInterval:(dayInSeconds*numberOfDays) sinceDate:firstDay];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K >= %@) AND (%K <= %@)",dayPropertyDate, firstDay, dayPropertyDate, lastDate];
    
    NSArray *resuls = [Day daysWithPredicate:predicate andMOC:moc];

    
    
    return resuls;
    
    
    
}

+ (BOOL)createWeekMenuInMOC:(NSManagedObjectContext *)moc withStartDate:(NSDate *)firstDay numberOfDays:(NSUInteger)numberDays{    
    [moc.undoManager beginUndoGrouping];
    BOOL created = [PGTMenuProvider menuForDays:numberDays inMOC:moc andStartDay:firstDay];
    [moc.undoManager endUndoGrouping];
    
    
    return created;
    
}



@end
