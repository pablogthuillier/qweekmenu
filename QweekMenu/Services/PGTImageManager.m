//
//  PGTImageManager.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 28/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTImageManager.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation PGTImageManager

-(void)saveMediaWithInfo:(NSDictionary *)info fileName:(NSString *)fileName completionBlock:(void(^)(UIImage *))completion{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *img = info[UIImagePickerControllerOriginalImage];
        
        ALAssetsLibrary *library = [ALAssetsLibrary new];
        dispatch_async(BACKGROUND_QUEUE, ^{
            [library writeImageToSavedPhotosAlbum:[img CGImage] orientation:(ALAssetOrientation)[img imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
                if (error) {
                    NSLog(@"error");
                    return;
                }
                
                [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                    
                    UIImage  *imgToSave = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
                    [self saveImage:imgToSave ToFileName:fileName];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(imgToSave);
                    });
                    
                    
                } failureBlock:^(NSError *error) {
                    NSLog(@"Failed to load captured image.");
                }];
            }];

        });
        
    }
}


-(void)saveImage:(UIImage *)image ToFileName:(NSString *)fileName {
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    
    NSString *saveFilePath = [docPath stringByAppendingPathComponent:fileName];
    NSData *binaryImageData = UIImagePNGRepresentation(image);
    
    [binaryImageData writeToFile:saveFilePath atomically:YES];

    
}

@end
