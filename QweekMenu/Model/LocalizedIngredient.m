//
//  LocalizedIngredient.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "LocalizedIngredient.h"
#import "Ingredient.h"


@implementation LocalizedIngredient

@dynamic languageCode;
@dynamic name;
@dynamic ingredient;

@end
