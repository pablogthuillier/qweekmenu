//
//  Ingredient.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IngredientDish, IngredientType, LocalizedIngredient;

@interface Ingredient : NSManagedObject

@property (nonatomic, retain) NSNumber * calories;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *dishes;
@property (nonatomic, retain) NSSet *localizedNames;
@property (nonatomic, retain) IngredientType *type;
@end

@interface Ingredient (CoreDataGeneratedAccessors)

- (void)addDishesObject:(IngredientDish *)value;
- (void)removeDishesObject:(IngredientDish *)value;
- (void)addDishes:(NSSet *)values;
- (void)removeDishes:(NSSet *)values;

- (void)addLocalizedNamesObject:(LocalizedIngredient *)value;
- (void)removeLocalizedNamesObject:(LocalizedIngredient *)value;
- (void)addLocalizedNames:(NSSet *)values;
- (void)removeLocalizedNames:(NSSet *)values;

@end
