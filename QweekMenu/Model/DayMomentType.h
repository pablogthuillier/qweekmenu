//
//  DayMomentType.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DayMoment;

@interface DayMomentType : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *dayMoments;
@end

@interface DayMomentType (CoreDataGeneratedAccessors)

- (void)addDayMomentsObject:(DayMoment *)value;
- (void)removeDayMomentsObject:(DayMoment *)value;
- (void)addDayMoments:(NSSet *)values;
- (void)removeDayMoments:(NSSet *)values;

@end
