//
//  Dish+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Dish.h"
#import "DishType+Model.h"
#import "Ingredient+Model.h"

extern NSString *const dishEntityName;
extern NSString *const dishPropertyName;
extern NSString *const dishPropertyPhoto;
extern NSString *const dishPropertyType;
extern NSString *const dishPropertyIngredients;
extern NSString *const dishPropertyLocalizedNames;
extern NSString *const localizedDishPropertyName;
extern NSString *const localizedDishPropertyLanguage;

@interface Dish (Model)

+ (instancetype) createDishInMoc:(NSManagedObjectContext *)moc;
+ (instancetype) dishInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict;
+ (instancetype) fetchInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;
+ (NSArray *)fetchAllDishesInMOC:(NSManagedObjectContext *)moc;
@end
