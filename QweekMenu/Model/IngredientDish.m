//
//  IngredientDish.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "IngredientDish.h"
#import "Dish.h"
#import "Ingredient.h"


@implementation IngredientDish

@dynamic priority;
@dynamic ingredient;
@dynamic dish;

@end
