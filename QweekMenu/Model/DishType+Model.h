//
//  DishType+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 15/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DishType.h"

@interface DishType (Model)

+ (instancetype) dishTypeInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;

@end
