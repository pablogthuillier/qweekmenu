//
//  DayMoment.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DayMoment.h"
#import "Day.h"
#import "DayMomentType.h"
#import "Dish.h"


@implementation DayMoment

@dynamic date;
@dynamic day;
@dynamic dishes;
@dynamic type;

@end
