//
//  IngredientDish+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "IngredientDish.h"

extern NSString *const ingredientDishEntityName;
extern NSString *const ingredientDishPropertyIngredient;
extern NSString *const ingredientDishPropertyPriority;

@interface IngredientDish (Model)

+ (void)createIngredientDishInMOC:(NSManagedObjectContext *)moc withIngredient:(Ingredient *)ingredient forDish:(Dish *)dish;

@end
