//
//  DayMomentType+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DayMomentType.h"

extern NSString *const dayMomentTypeEntityName;
extern NSString *const dayMomentTypePropertyName;

extern NSString *const lunchName;
extern NSString *const dinnerName;

@interface DayMomentType (Model)


+ (instancetype) dayMomentTypeInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;
+ (instancetype) fetchInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;

@end
