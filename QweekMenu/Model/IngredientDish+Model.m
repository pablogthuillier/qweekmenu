//
//  IngredientDish+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "IngredientDish+Model.h"

NSString *const ingredientDishEntityName = @"IngredientDish";
NSString *const ingredientDishPropertyIngredient = @"ingredient";
NSString *const ingredientDishPropertyPriority = @"priority";
NSString *const ingredientDishPropertyDish = @"dish";

@implementation IngredientDish (Model)

+ (void)createIngredientDishInMOC:(NSManagedObjectContext *)moc withIngredient:(Ingredient *)ingredient forDish:(Dish *)dish{
    IngredientDish *item = [NSEntityDescription insertNewObjectForEntityForName:ingredientDishEntityName
                                             inManagedObjectContext:moc];
    item.dish = dish;
    item.ingredient = ingredient;
    item.priority = @1;

}

@end
