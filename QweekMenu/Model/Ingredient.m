//
//  Ingredient.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Ingredient.h"
#import "IngredientDish.h"
#import "IngredientType.h"
#import "LocalizedIngredient.h"


@implementation Ingredient

@dynamic calories;
@dynamic name;
@dynamic dishes;
@dynamic localizedNames;
@dynamic type;

@end
