//
//  Tag.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Tag.h"
#import "Dish.h"


@implementation Tag

@dynamic name;
@dynamic dishes;

@end
