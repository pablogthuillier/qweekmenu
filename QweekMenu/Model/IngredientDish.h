//
//  IngredientDish.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Dish, Ingredient;

@interface IngredientDish : NSManagedObject

@property (nonatomic, retain) NSNumber * priority;
@property (nonatomic, retain) Ingredient *ingredient;
@property (nonatomic, retain) Dish *dish;

@end
