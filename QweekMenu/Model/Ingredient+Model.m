//
//  Ingredient+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Ingredient+Model.h"
#import "IngredientType+Model.h"
#import "LocalizedIngredient.h"

NSString *const ingredientEntityName = @"Ingredient";
NSString *const ingredientPropertyName = @"name";
NSString *const ingredientPropertyType = @"type";
NSString *const ingredientPropertyCalories = @"calories";
NSString *const ingredientPropertyLocalizedNames = @"localizedNames";
NSString *const localizedIngredientEntityName = @"LocalizedIngredient";
NSString *const localizedIngredientPropertyName = @"name";
NSString *const localizedIngredientPropertyLanguage = @"languageCode";

@implementation Ingredient (Model)

+ (instancetype) ingredientInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict {
    
    Ingredient *ingredient = nil;
    
    NSString *name = dict[ingredientPropertyName];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:ingredientEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", ingredientPropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if([results count]){
        ingredient = [results firstObject];
    }
    
    else{
        ingredient = [NSEntityDescription insertNewObjectForEntityForName:ingredientEntityName
                                                   inManagedObjectContext:moc];
        ingredient.name = name;
        if(dict[ingredientPropertyCalories]){
            ingredient.calories = dict[ingredientPropertyCalories];
        }
        if([dict[ingredientPropertyLocalizedNames] isKindOfClass:[NSArray class]]){
            for (NSDictionary *dictLocalized in dict[ingredientPropertyLocalizedNames]) {
                LocalizedIngredient *localIng = [NSEntityDescription insertNewObjectForEntityForName:localizedIngredientEntityName
                                                                              inManagedObjectContext:moc];
                localIng.languageCode = dictLocalized[localizedIngredientPropertyLanguage];
                localIng.name = dictLocalized[localizedIngredientPropertyName];
                [ingredient addLocalizedNamesObject:localIng];
            }
        }
        NSDictionary *dictType = @{ingredientTypePropertyName:dict[ingredientPropertyType]};
        IngredientType *type = [IngredientType ingredientTypeInMOC:moc withDictionary:dictType];
        ingredient.type = type;
        
    }
    
    
    return ingredient;
}


+ (instancetype) fetchInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:ingredientEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", ingredientPropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    return [results lastObject];
}


+ (NSArray *)fetchAllIngredientsInMOC:(NSManagedObjectContext *)moc{
    
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:ingredientEntityName];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if(![language isEqualToString:@"en"]){
        
        
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:ingredientPropertyName
                                                                   ascending:YES];
    [fetch setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    [fetch setReturnsObjectsAsFaults:NO];
    
    return [moc executeFetchRequest:fetch error:nil];
}

@end
