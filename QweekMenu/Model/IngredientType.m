//
//  IngredientType.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "IngredientType.h"
#import "Ingredient.h"


@implementation IngredientType

@dynamic name;
@dynamic ingredients;

@end
