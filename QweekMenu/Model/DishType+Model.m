//
//  DishType+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 15/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DishType+Model.h"

NSString *const dishTypeEntityName = @"DishType";
NSString *const dishTypePropertyName = @"name";

@implementation DishType (Model)

+ (instancetype) dishTypeInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    DishType *item = nil;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dishTypeEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", dishTypePropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if([results count]){
        item = [results firstObject];
    }
    
    else{
        item = [NSEntityDescription insertNewObjectForEntityForName:dishTypeEntityName
                                                      inManagedObjectContext:moc];
        item.name = name;
    }
    
    
    return item;
}

@end
