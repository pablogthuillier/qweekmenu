//
//  DayMoment.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Day, DayMomentType, Dish;

@interface DayMoment : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) Day *day;
@property (nonatomic, retain) NSSet *dishes;
@property (nonatomic, retain) DayMomentType *type;
@end

@interface DayMoment (CoreDataGeneratedAccessors)

- (void)addDishesObject:(Dish *)value;
- (void)removeDishesObject:(Dish *)value;
- (void)addDishes:(NSSet *)values;
- (void)removeDishes:(NSSet *)values;

@end
