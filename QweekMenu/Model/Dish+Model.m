//
//  Dish+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Dish+Model.h"
#import "LocalizedDish.h"
#import "IngredientDish.h"
#import "IngredientDish+Model.h"

NSString *const dishEntityName = @"Dish";
NSString *const dishPropertyName = @"name";
NSString *const dishPropertyPhoto = @"photoURL";
NSString *const dishPropertyType = @"type";
NSString *const dishPropertyIngredients = @"ingredients";
NSString *const dishPropertyLocalizedNames = @"localizedNames";
NSString *const localizedDishEntityName = @"LocalizedDish";
NSString *const localizedDishPropertyName = @"name";
NSString *const localizedDishPropertyLanguage = @"languageCode";

@implementation Dish (Model)

+ (instancetype) createDishInMoc:(NSManagedObjectContext *)moc{
    Dish *item = nil;
    item = [NSEntityDescription insertNewObjectForEntityForName:dishEntityName
                                         inManagedObjectContext:moc];
    
    item.name = @"";
    
    return item;
}

+ (instancetype) dishInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict {
    Dish *item = nil;
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *name = dict[dishPropertyName];
    if(![language isEqualToString:DEFAULT_LANGUAGE_CODE]){
        for (NSDictionary *localDish in [dict valueForKey:dishPropertyLocalizedNames]) {
            if([[localDish objectForKey:@"languageCode"] isEqualToString:language]){
                name = [localDish objectForKey:dishPropertyName];
            }
        }
    }
    
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dishEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", dishPropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if([results count]){
        item = [results firstObject];
    }
    
    else{
        item = [NSEntityDescription insertNewObjectForEntityForName:dishEntityName
                                             inManagedObjectContext:moc];
        item.name = name;
        
        DishType *dishType = [DishType dishTypeInMOC:moc withName:dict[dishPropertyType]];
        item.type = dishType;
        
        NSMutableSet *ingredients = [[NSMutableSet alloc] init];
        if([dict[dishPropertyIngredients] isKindOfClass:[NSArray class]]){
            for (NSDictionary *ingredientDict in dict[dishPropertyIngredients]) {
                Ingredient *ingredient = [Ingredient ingredientInMOC:moc withDictionary:ingredientDict];
                IngredientDish *ingredientDish = [NSEntityDescription insertNewObjectForEntityForName:ingredientDishEntityName inManagedObjectContext:moc];
                ingredientDish.ingredient = ingredient;
                ingredientDish.priority = @1;
                [ingredients addObject:ingredientDish];
            }
        }
        
        if([dict[dishPropertyLocalizedNames] isKindOfClass:[NSArray class]]){
            for (NSDictionary *dictLocalized in dict[dishPropertyLocalizedNames]) {
                LocalizedDish *localDish = [NSEntityDescription insertNewObjectForEntityForName:localizedDishEntityName
                                                                              inManagedObjectContext:moc];
                localDish.languageCode = dictLocalized[localizedDishPropertyLanguage];
                localDish.name = dictLocalized[localizedDishPropertyName];
                [item addLocalizedNamesObject:localDish];
            }
        }
        
        if(dict[dishPropertyPhoto]){
            item.photoUrl = dict[dishPropertyPhoto];
        }
        
        item.ingredients = ingredients;

    }
    
    
    return item;
}


+ (instancetype) fetchInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dishEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", dishPropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    return [results lastObject];
}

+ (NSArray *)fetchAllDishesInMOC:(NSManagedObjectContext *)moc {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dishEntityName];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:dishPropertyName
                                                                   ascending:YES];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

@end
