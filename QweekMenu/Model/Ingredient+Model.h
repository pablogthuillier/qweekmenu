//
//  Ingredient+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Ingredient.h"

extern NSString *const ingredientEntityName;
extern NSString *const ingredientPropertyName;
extern NSString *const ingredientPropertyType;
extern NSString *const ingredientPropertyCalories;
extern NSString *const ingredientPropertyLocalizedNames;

@interface Ingredient (Model)

+ (instancetype) ingredientInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict;
+ (instancetype) fetchInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name;
+ (NSArray *)fetchAllIngredientsInMOC:(NSManagedObjectContext *)moc;


@end
