//
//  Dish.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DayMoment, DishType, IngredientDish, LocalizedDish, Tag;

@interface Dish : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * photoUrl;
@property (nonatomic, retain) NSSet *dayMoments;
@property (nonatomic, retain) NSSet *ingredients;
@property (nonatomic, retain) NSSet *localizedNames;
@property (nonatomic, retain) DishType *type;
@property (nonatomic, retain) NSSet *tags;
@end

@interface Dish (CoreDataGeneratedAccessors)

- (void)addDayMomentsObject:(DayMoment *)value;
- (void)removeDayMomentsObject:(DayMoment *)value;
- (void)addDayMoments:(NSSet *)values;
- (void)removeDayMoments:(NSSet *)values;

- (void)addIngredientsObject:(IngredientDish *)value;
- (void)removeIngredientsObject:(IngredientDish *)value;
- (void)addIngredients:(NSSet *)values;
- (void)removeIngredients:(NSSet *)values;

- (void)addLocalizedNamesObject:(LocalizedDish *)value;
- (void)removeLocalizedNamesObject:(LocalizedDish *)value;
- (void)addLocalizedNames:(NSSet *)values;
- (void)removeLocalizedNames:(NSSet *)values;

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

@end
