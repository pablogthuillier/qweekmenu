//
//  Dish.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Dish.h"
#import "DayMoment.h"
#import "DishType.h"
#import "IngredientDish.h"
#import "LocalizedDish.h"
#import "Tag.h"


@implementation Dish

@dynamic name;
@dynamic photoUrl;
@dynamic dayMoments;
@dynamic ingredients;
@dynamic localizedNames;
@dynamic type;
@dynamic tags;

@end
