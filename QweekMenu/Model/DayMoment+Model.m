//
//  DayMoment+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DayMoment+Model.h"
#import "Day+Model.h"

NSString *const dayMomentEntityName = @"DayMoment";
NSString *const dayMomentPropertyDate = @"date";
NSString *const dayMomentPropertyTypeName = @"type";

@implementation DayMoment (Model)

+ (instancetype) dayMomentInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict rewrite:(BOOL)rewrite {
    DayMoment *dayMoment = nil;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dayMomentEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(date == %@) AND (type.name == %@)",dict[dayMomentPropertyDate], dict[dayMomentPropertyTypeName]];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];

    
    if([results count]){
        dayMoment = [results firstObject];
        if(rewrite){
            dayMoment.date = dict[dayMomentPropertyDate];
            
            DayMomentType *type = [DayMomentType dayMomentTypeInMOC:moc withName:dict[dayMomentPropertyTypeName]];
            dayMoment.type = type;
        }
    }
    
    else{
        Day *day = [Day dayInMOC:moc withDate:dict[dayMomentPropertyDate]];
        
        dayMoment = [NSEntityDescription insertNewObjectForEntityForName:dayMomentEntityName
                                                   inManagedObjectContext:moc];
        dayMoment.date = dict[dayMomentPropertyDate];
        dayMoment.day = day;
        
        DayMomentType *type = [DayMomentType dayMomentTypeInMOC:moc withName:dict[dayMomentPropertyTypeName]];
        dayMoment.type = type;
    }
    
    
    return dayMoment;
}


+ (instancetype)createDayMomentInMOC:(NSManagedObjectContext *)moc
                            withDish:(Dish *)dish
                             date:(NSDate *)date
                             andType:(NSString *)type
                             replace:(BOOL)replace{
    
    DayMoment *day = [self dayMomentInMOC:moc withDictionary:@{dayMomentPropertyDate:date, dayMomentPropertyTypeName:type} rewrite:YES];
    if(replace){
        [day removeDishes:[day dishes]];
    }
    if(dish){
      [day addDishesObject:dish];  
    }
    
    
    return day;
    
}



@end
