//
//  DayMomentType+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DayMomentType+Model.h"

NSString *const lunchName = @"lunch";
NSString *const dinnerName = @"dinner";

NSString *const dayMomentTypeEntityName = @"DayMomentType";
NSString *const dayMomentTypePropertyName = @"name";

@implementation DayMomentType (Model)

+ (instancetype) dayMomentTypeInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    DayMomentType *dayMomentType = nil;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dayMomentTypeEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", dayMomentTypePropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if([results count]){
        dayMomentType = [results firstObject];
    }
    
    else{
        dayMomentType = [NSEntityDescription insertNewObjectForEntityForName:dayMomentTypeEntityName
                                                   inManagedObjectContext:moc];
        dayMomentType.name = name;
    }
    
    
    return dayMomentType;
}


+ (instancetype) fetchInMOC:(NSManagedObjectContext *)moc withName:(NSString *)name {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dayMomentTypeEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", dayMomentTypePropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    return [results lastObject];
}

@end
