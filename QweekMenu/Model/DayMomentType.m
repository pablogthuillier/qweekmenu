//
//  DayMomentType.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DayMomentType.h"
#import "DayMoment.h"


@implementation DayMomentType

@dynamic name;
@dynamic dayMoments;

@end
