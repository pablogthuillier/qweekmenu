//
//  Day.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DayMoment;

@interface Day : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSSet *moments;
@end

@interface Day (CoreDataGeneratedAccessors)

- (void)addMomentsObject:(DayMoment *)value;
- (void)removeMomentsObject:(DayMoment *)value;
- (void)addMoments:(NSSet *)values;
- (void)removeMoments:(NSSet *)values;

@end
