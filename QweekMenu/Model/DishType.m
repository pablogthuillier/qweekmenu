//
//  DishType.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DishType.h"
#import "Dish.h"


@implementation DishType

@dynamic name;
@dynamic dishes;

@end
