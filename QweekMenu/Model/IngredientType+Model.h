//
//  IngredientType+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "IngredientType.h"

extern NSString *const ingredientTypeEntityName;
extern NSString *const ingredientTypePropertyName;

@interface IngredientType (Model)

+ (instancetype) ingredientTypeInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict;

@end
