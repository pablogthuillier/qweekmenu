//
//  LocalizedDish.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "LocalizedDish.h"
#import "Dish.h"


@implementation LocalizedDish

@dynamic languageCode;
@dynamic name;
@dynamic dish;

@end
