//
//  IngredientType+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "IngredientType+Model.h"

NSString *const ingredientTypeEntityName = @"IngredientType";
NSString *const ingredientTypePropertyName = @"name";

@implementation IngredientType (Model)

+ (instancetype) ingredientTypeInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict{
    IngredientType *item = nil;
    
    NSString *name = dict[ingredientTypePropertyName];
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:ingredientTypeEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", ingredientTypePropertyName, name];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if([results count]){
        item = [results firstObject];
    }
    
    else{
        item = [NSEntityDescription insertNewObjectForEntityForName:ingredientTypeEntityName
                                                   inManagedObjectContext:moc];
        item.name = name;
        
    }
    
    
    return item;
    
}

@end