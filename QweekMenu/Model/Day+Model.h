//
//  Day+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Day.h"

extern NSString *const dayEntityName;
extern NSString *const dayPropertyDate;

@interface Day (Model)

+ (instancetype) dayInMOC:(NSManagedObjectContext *)moc withDate:(NSDate *)date;
+ (instancetype) fetchDayInMOC:(NSManagedObjectContext *)moc withDate:(NSDate *)date;
+ (NSArray *)daysWithPredicate:(NSPredicate *)predicate andMOC:(NSManagedObjectContext *)moc;

@end
