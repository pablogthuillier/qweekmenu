//
//  Day+Model.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Day+Model.h"

NSString *const dayEntityName = @"Day";
NSString *const dayPropertyDate = @"date";

@implementation Day (Model)

+ (instancetype) dayInMOC:(NSManagedObjectContext *)moc withDate:(NSDate *)date{
    Day *day = [self fetchDayInMOC:moc withDate:date];
    if(!day){
        day = [NSEntityDescription insertNewObjectForEntityForName:dayEntityName
                                                      inManagedObjectContext:moc];
        day.date = date;
    }
    
    
    return day;

    
}

+ (instancetype) fetchDayInMOC:(NSManagedObjectContext *)moc withDate:(NSDate *)date{
    Day *day = nil;
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dayEntityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", dayPropertyDate, date];
    
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    
    if([results count]){
        day = [results firstObject];
    }
    
    return day;
    
    
}

+ (NSArray *)daysWithPredicate:(NSPredicate *)predicate andMOC:(NSManagedObjectContext *)moc {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:dayEntityName];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:dayPropertyDate ascending:YES]];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *array = [moc executeFetchRequest:fetchRequest error:&error];
    
    return array;
}

@end
