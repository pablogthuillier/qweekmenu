//
//  DayMoment+Model.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "DayMoment.h"
#import "DayMomentType+Model.h"

extern NSString *const dayMomentEntityName;
extern NSString *const dayMomentPropertyDate;
extern NSString *const dayMomentPropertyTypeName;

@interface DayMoment (Model)

+ (instancetype) dayMomentInMOC:(NSManagedObjectContext *)moc withDictionary:(NSDictionary *)dict rewrite:(BOOL)rewrite;
+ (instancetype)createDayMomentInMOC:(NSManagedObjectContext *)moc
                            withDish:(Dish *)dish
                             date:(NSDate *)date
                             andType:(NSString *)type
                             replace:(BOOL)replace;

@end
