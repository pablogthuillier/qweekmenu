//
//  Day.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "Day.h"
#import "DayMoment.h"


@implementation Day

@dynamic date;
@dynamic moments;

@end
