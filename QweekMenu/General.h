//
//  General.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#ifndef QweekMenu_General_h
#define QweekMenu_General_h

#define WIDGET_GROUP_NAME @"group.thuillier.QweekMenu"
#define WIDGET_SUMMARY_KEY @"menuSummary"

#define SETTINGS_LEGUMES_NAME @"avoidLegumesAtNight"
#define SETTINGS_MEAT_MAX_NAME @"meatMax"
#define SETTINGS_CEREALS_MAX_NAME @"cerealsMax"
#define SETTINGS_LAST_DATE @"lastDate"
#define DEFAULT_LANGUAGE_CODE @"en"
#define SPANISH_LANGUAGE_CODE @"es"

#define DATE_CHANGE_NOTIFICATION @"DateChangeNotification"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define SYSTEM_FONT_NAME @"Helvetica"
#define CUSTOM_FONT_NAME @"Heiti TC"

#define CUSTOM_FONT_NORMAL_LABEL [UIFont fontWithName:CUSTOM_FONT_NAME size:17.0]
#define CUSTOM_FONT_SMALL_LABEL [UIFont fontWithName:CUSTOM_FONT_NAME size:13.0]
#define CUSTOM_FONT_BIG_LABEL [UIFont fontWithName:CUSTOM_FONT_NAME size:32.0]

//GIANT GOLDFISH (http://pltts.me/palettes/92095)
#define COLOR_BLUE_DARK @"#69D2E7"
#define COLOR_BLUE_LIGHT @"#A7DBD8"
//#define COLOR_NEUTRAL @"#E0E4CC"
#define COLOR_NEUTRAL @"#EBE9BC"
#define COLOR_ORANGE_LIGHT @"#F38630"
#define COLOR_ORANGE_DARK @"#FA6900"

#define COLOR_DARK_BROWN @"#BABD8B"


#define BACKGROUND_QUEUE dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)



#endif
