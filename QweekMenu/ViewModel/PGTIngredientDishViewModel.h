//
//  PGTIngredientDishViewModel.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IngredientDish.h"

@interface PGTIngredientDishViewModel : NSObject

@property (nonatomic,strong) IngredientDish *ingredientDish;

- (NSString *)ingredientName;
- (NSString *)ingredientType;

@end
