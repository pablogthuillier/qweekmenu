//
//  PGTEditDishViewModel.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 17/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dish+Model.h"
#import "Day+Model.h"

@interface PGTDishViewModel : NSObject

@property (nonatomic,strong) UIManagedDocument *managedDocument;


@property (nonatomic,strong) Dish *dish;
@property (nonatomic,copy) NSArray *ingredients;
@property (nonatomic,copy) NSArray *dishIngredientsArray;
@property (nonatomic,copy) NSArray *dishIngredients;

@property (nonatomic) NSInteger dishIngredientsCount;
@property (nonatomic,copy) NSString *dishName;
@property (nonatomic,strong) UIImage *dishImage;

- (NSArray *)dishIngredients;
- (NSSet *)ingredientsSet;
- (NSArray *)totalIngredients;
- (UIImage *)defaultDishImage;
- (NSArray *)dishIngredientsArray;

- (BOOL)saveDishDataWithName:(NSString *)name;
- (BOOL)saveDishIngredients:(NSArray *)ingredients;
- (BOOL)saveDishDataWithImage:(NSString *)imageFilePath;
- (BOOL)saveDishIngredients;



- (void)saveDishDataWithName:(NSString *)name andImageURL:(NSString *)imageFilePath;

@end
