//
//  PGTIngredientViewModel.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ingredient+Model.h"

@interface PGTIngredientViewModel : NSObject

@property (nonatomic,strong) Ingredient *ingredient;

- (NSString *)ingredientName;
- (NSString *)ingredientType;

@end
