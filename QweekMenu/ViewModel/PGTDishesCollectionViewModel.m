//
//  PGTDishesCollectionViewModel.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDishesCollectionViewModel.h"
#import "Dish+Model.h"

@implementation PGTDishesCollectionViewModel

- (NSArray *)allDishes{
    NSArray *results = [Dish fetchAllDishesInMOC:self.managedDocument.managedObjectContext];
    return results;
}



@end
