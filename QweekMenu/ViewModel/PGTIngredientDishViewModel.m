//
//  PGTIngredientDishViewModel.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTIngredientDishViewModel.h"
#import "PGTIngredientViewModel.h"

@interface PGTIngredientDishViewModel ()

@property (nonatomic,strong) PGTIngredientViewModel *ingredientViewModel;

@end

@implementation PGTIngredientDishViewModel

- (PGTIngredientViewModel *)ingredientViewModel{
    if(!_ingredientViewModel){
        _ingredientViewModel = [[PGTIngredientViewModel alloc] init];
        _ingredientViewModel.ingredient = self.ingredientDish.ingredient;
    }
    
    return _ingredientViewModel;
}


- (NSString *)ingredientName{
    return [self.ingredientViewModel ingredientName];
}

- (NSString *)ingredientType{
    return [self.ingredientViewModel ingredientType];
    
}

@end
