//
//  PGTDayViewModel.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Day+Model.h"

@interface PGTDayViewModel : NSObject

@property (nonatomic,strong) Day *day;
@property (readonly,strong) NSDate *date;

- (NSString *)dishNameWithDayMomentType:(NSString *)type;
- (NSArray *)dishWithDayMomentType:(NSString *)type;
- (NSArray *)dishWithDayMomentTypeIndex:(NSUInteger)typeIndex;
- (NSUInteger)numberOfDayMoments;
- (NSString *)dayMomentNameWithDayMomentTypeIndex:(NSUInteger)typeIndex;

@end
