//
//  PGTIngredientViewModel.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTIngredientViewModel.h"
#import "IngredientType+Model.h"
#import "LocalizedIngredient.h"
#import "IngredientDish.h"

@implementation PGTIngredientViewModel

- (NSString *)ingredientName {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *name;
    if(![language isEqualToString:DEFAULT_LANGUAGE_CODE]){
        for (LocalizedIngredient *localIng in self.ingredient.localizedNames) {
            if([localIng.languageCode isEqualToString:language]){
                name = localIng.name;
            }
        }
    }
    
    if(!name){
        name = self.ingredient.name;
    }
    
    return name;
    
    
}


- (NSString *)ingredientType{
    return self.ingredient.type.name;
}

@end
