//
//  PGTEditDishViewModel.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 17/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDishViewModel.h"
#import "LocalizedDish.h"
#import "IngredientDish+Model.h"
#import "LocalizedIngredient.h"

NSString *const defaultIngredientImage = @"05.jpg";

@implementation PGTDishViewModel

#pragma mark - Getters


- (NSInteger)dishIngredientsCount{
    return [self.dishIngredients count];
}

- (NSString *)dishName{
    return [self.dish.name copy];

}

- (UIImage *)dishImage{
    UIImage *image = nil;
    if(self.dish.photoUrl && ![self.dish.photoUrl isEqualToString:@""]){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithString:self.dish.photoUrl] ];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]){
            image = [UIImage imageWithContentsOfFile:path];
        }
        else{
            image = [UIImage imageNamed:self.dish.photoUrl];
        }
        
    }
    
    return image;
}

- (UIImage *)defaultDishImage {
    return [UIImage imageNamed:defaultIngredientImage];
}

- (NSArray *)dishIngredientsArray{
    if(!_dishIngredientsArray){
        
        NSArray *sortedIngredientsDish = [self sortIngredientsDish:[self.dish.ingredients allObjects]];
        
        NSMutableArray *sortedIngredients = [[NSMutableArray alloc] init];
        for (IngredientDish *ingredientDish in sortedIngredientsDish) {
            [sortedIngredients addObject:ingredientDish.ingredient];
        }
        
        _dishIngredientsArray = sortedIngredients;
    }


    return _dishIngredientsArray;
}

- (NSArray *)sortIngredientsDish:(NSArray *)asortedIngredientsDish {
    NSArray *sortedIngredientsDish;
    sortedIngredientsDish = [asortedIngredientsDish sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        IngredientDish *first = (IngredientDish*)a;
        IngredientDish *second = (IngredientDish*)b;
        
        NSString *firstName = [self localizedIngredientName:first.ingredient];
        NSString *secondName = [self localizedIngredientName:second.ingredient];
        
        return [firstName compare:secondName];
    }];
    
    return sortedIngredientsDish;
}

- (NSArray *)sortIngredients:(NSArray *)asortedIngredients {
    NSArray *sortedIngredients;
    sortedIngredients = [asortedIngredients sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        Ingredient *first = (Ingredient*)a;
        Ingredient *second = (Ingredient*)b;
        
        NSString *firstName = [self localizedIngredientName:first];
        NSString *secondName = [self localizedIngredientName:second];

        
        return [firstName compare:secondName];
    }];
    
    return sortedIngredients;
}

- (NSString *)localizedIngredientName:(Ingredient *)ingredient {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *name = ingredient.name;
    if(![language isEqualToString:DEFAULT_LANGUAGE_CODE]){
        for (LocalizedIngredient *localIng in ingredient.localizedNames) {
            if([localIng.languageCode isEqualToString:language]){
                name = localIng.name;
            }
        }
    }
    
    return name;
}


- (NSArray *)dishIngredients{
    if(!_dishIngredients){
        NSArray *sortedIngredients = [self sortIngredientsDish:[self.dish.ingredients allObjects]];
        _dishIngredients = [sortedIngredients copy];
    }
    return _dishIngredients;
}


- (NSSet *)ingredientsSet{
    return [NSSet setWithArray:self.ingredients];
}

- (NSArray *)totalIngredients {
    NSArray *asortedIngredients = [Ingredient fetchAllIngredientsInMOC:self.managedDocument.managedObjectContext];
    NSArray *sortedIngredients = [self sortIngredients:asortedIngredients];
    return sortedIngredients;
}


#pragma mark - Save data

- (BOOL)saveDishIngredients:(NSArray *)ingredients{
    if(ingredients){
        self.dishIngredientsArray = ingredients;
        [self saveDishIngredients];
    }
    
    return YES;
}

- (BOOL)saveDishDataWithName:(NSString *)name{
    self.dish.name = name;
    return YES;
}


- (BOOL)saveDishIngredients{
    NSMutableArray *tmpIngredientsArray = [self.dishIngredientsArray mutableCopy];
    for (IngredientDish *initialIngredientDish in self.dish.ingredients) {
        BOOL ingredientFound = NO;
        for (Ingredient *ingredient in self.dishIngredientsArray) {
            if([ingredient isEqual:initialIngredientDish.ingredient]){
                ingredientFound = YES;
                [tmpIngredientsArray removeObject:ingredient];
            }
        }
        if (!ingredientFound){
            [self.managedDocument.managedObjectContext deleteObject:initialIngredientDish];
        }
    }
    
    
    for (Ingredient *ingredient in tmpIngredientsArray) {
        [IngredientDish createIngredientDishInMOC:self.managedDocument.managedObjectContext
                                   withIngredient:ingredient
                                          forDish:self.dish];
    }

    self.dishIngredients = nil;
    self.dishIngredientsArray = nil;

    return YES;
}

- (BOOL)saveDishDataWithImage:(NSString *)imageFilePath{
    if(imageFilePath){
        self.dish.photoUrl = imageFilePath;
    }
    return YES;
}

- (void)saveDishDataWithName:(NSString *)name andImageURL:(NSString *)imageFilePath{
    if(!self.dish){
        self.dish = [Dish createDishInMoc:self.managedDocument.managedObjectContext];
    }
    [self saveDishDataWithName:name];
    [self saveDishIngredients];
    [self saveDishDataWithImage:imageFilePath];
}


@end
