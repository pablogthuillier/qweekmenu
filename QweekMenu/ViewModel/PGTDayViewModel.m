//
//  PGTDayViewModel.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDayViewModel.h"
#import "Dish+Model.h"
#import "DayMoment+Model.h"
#import "PGTDishViewModel.h"

@implementation PGTDayViewModel


//Use of settings manager instead of lunch and dinner as constants

- (NSString *)dishNameWithDayMomentType:(NSString *)type {
    Dish *dish = [[self dishWithDayMomentType:type] firstObject];
    PGTDishViewModel *viewModel = [[PGTDishViewModel alloc] init];
    viewModel.dish = dish;
    return [viewModel dishName];
}


- (NSArray *)dishWithDayMomentTypeIndex:(NSUInteger)typeIndex {
    NSString *type = [self dayMomentNameWithDayMomentTypeIndex:typeIndex];
    Dish *dishFound = nil;
    NSMutableArray *dishes = [[NSMutableArray alloc] init];
    NSArray *dayMoments = [self.day.moments allObjects];
    for (DayMoment *dayMoment in dayMoments) {
        if([dayMoment.type.name isEqual:type]){
            dishFound = [[[dayMoment dishes] allObjects] firstObject];
            if(dishFound){
                [dishes addObject:dishFound];
            }
            
        }
        
    }
    
    
    return [dishes copy];
}

- (NSArray *)dishWithDayMomentType:(NSString *)type {
    Dish *dishFound = nil;
    NSMutableArray *dishes = [[NSMutableArray alloc] init];
    NSArray *dayMoments = [self.day.moments allObjects];
    for (DayMoment *dayMoment in dayMoments) {
        if([dayMoment.type.name isEqual:type]){
            dishFound = [[[dayMoment dishes] allObjects] firstObject];
            if(dishFound){
                [dishes addObject:dishFound];
            }
            
        }
        
    }
    
    
    return [dishes copy];
}

- (NSUInteger)numberOfDayMoments {
    return [self.day.moments count];
}

- (NSString *)dayMomentNameWithDayMomentTypeIndex:(NSUInteger)typeIndex {
    return typeIndex == 0 ? lunchName : dinnerName;
}

- (NSDate *)date{
    return self.day.date;
}


@end
