//
//  PGTDishesCollectionViewModel.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGTDishesCollectionViewModel : NSObject

@property (nonatomic,strong) UIManagedDocument *managedDocument;

- (NSArray *)allDishes;

@end
