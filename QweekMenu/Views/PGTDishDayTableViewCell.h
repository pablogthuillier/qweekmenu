//
//  PGTDishDayTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGTDishViewModel.h"


@protocol PGTDishDayCellDelegate <NSObject>

- (void)openDishEditorWithViewModel:(PGTDishViewModel *)viewModel;

@end

@interface PGTDishDayTableViewCell : UITableViewCell

@property (nonatomic,strong) PGTDishViewModel *viewModel;
@property (nonatomic,strong) id<PGTDishDayCellDelegate> delegate;


@end
