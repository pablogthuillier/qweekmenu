//
//  PGTChooseDishTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGTDishViewModel.h"

@interface PGTChooseDishTableViewCell : UITableViewCell

@property (nonatomic,strong) PGTDishViewModel *vieModel;

@end
