//
//  PGTEditDishTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTEditDishTableViewCell.h"

@implementation PGTEditDishTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [self setupView];
}

- (void)setViewModel:(PGTIngredientDishViewModel *)viewModel{
    _viewModel = viewModel;
    [self loadData];
    
}

- (void)loadData {
    self.textLabel.text = [self.viewModel ingredientName];
    
}

- (void)setupView {
    
    self.textLabel.font = CUSTOM_FONT_NORMAL_LABEL;
    
}

@end
