//
//  PGTImportedDishTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 03/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PGTImportedDishTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *dishImageView;
@property (weak, nonatomic) IBOutlet UILabel *dishName;

@end
