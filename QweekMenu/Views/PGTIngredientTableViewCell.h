//
//  PGTIngredientTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 24/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ingredient+Model.h"
#import "PGTIngredientViewModel.h"

@interface PGTIngredientTableViewCell : UITableViewCell

@property (nonatomic,strong) PGTIngredientViewModel *viewModel;

@end
