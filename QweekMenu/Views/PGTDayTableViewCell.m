//
//  PGTDayTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 18/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDayTableViewCell.h"
#import "Dish+Model.h"
#import "PGTDateManager.h"


@interface PGTDayTableViewCell ()
@property (weak, nonatomic) IBOutlet UIView *circleViewDay;
@property (weak, nonatomic) IBOutlet UILabel *weekDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *lunchDishName;
@property (weak, nonatomic) IBOutlet UILabel *dinnerDishName;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation PGTDayTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    [self setupView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setViewModel:(PGTDayViewModel *)viewModel{
    _viewModel = viewModel;
    [self loadData];
    
    
    
}

- (void)loadData {
    self.dateLabel.text = [PGTDateManager dateWithDayAndMonth:self.viewModel.day.date];
    
    self.lunchDishName.text = [[self.viewModel dishNameWithDayMomentType:lunchName] capitalizedString];
    self.dinnerDishName.text = [[self.viewModel dishNameWithDayMomentType:dinnerName] capitalizedString];
    
    NSUInteger dayOfWeek = [PGTDateManager dayOfTheWeekFromDate:self.viewModel.day.date];
    
    self.weekDayLabel.text = [PGTDateManager firstCharOfDay:dayOfWeek];
    
}

- (void)setupView {
    
    [self.circleViewDay.layer setCornerRadius:40];
    
    self.weekDayLabel.font = CUSTOM_FONT_BIG_LABEL;
    self.dateLabel.font = CUSTOM_FONT_SMALL_LABEL;
    self.lunchDishName.font = CUSTOM_FONT_NORMAL_LABEL;
    
    

}


@end
