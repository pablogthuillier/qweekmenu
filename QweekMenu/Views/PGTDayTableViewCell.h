//
//  PGTDayTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 18/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Day+Model.h"
#import "DayMoment+Model.h"
#import "Dish+Model.h"
#import "PGTDayViewModel.h"

@interface PGTDayTableViewCell : UITableViewCell

@property (nonatomic,strong) PGTDayViewModel *viewModel;
@property (nonatomic) BOOL emphasize;

@end
