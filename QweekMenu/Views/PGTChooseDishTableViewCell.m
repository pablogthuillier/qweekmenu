//
//  PGTChooseDishTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 23/09/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTChooseDishTableViewCell.h"

@interface PGTChooseDishTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *dishImageView;
@property (weak, nonatomic) IBOutlet UILabel *dishNameLabel;

@end

@implementation PGTChooseDishTableViewCell

- (void)setVieModel:(PGTDishViewModel *)vieModel{
    self.dishNameLabel.text = vieModel.dishName;
    self.dishImageView.image = vieModel.dishImage;
}

- (void)awakeFromNib{
    [self.dishNameLabel setFont:CUSTOM_FONT_SMALL_LABEL];
}

@end
