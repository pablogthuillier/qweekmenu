//
//  PGTDishDayTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 20/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDishDayTableViewCell.h"
#import "IngredientType+Model.h"
#import "IngredientDish+Model.h"

@interface PGTDishDayTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *dishImageView;
@property (weak, nonatomic) IBOutlet UITextField *dishName;
@property (weak, nonatomic) IBOutlet UIImageView *editButton;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg1;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg2;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg3;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg4;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg5;

@end

@implementation PGTDishDayTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setViewModel:(PGTDishViewModel *)viewModel{
    _viewModel = viewModel;
    
    [self setupView];
    [self loadData];
    
}

- (void)loadData {
    self.dishImageView.image = self.dishImageView.image = self.viewModel.dishImage ? self.viewModel.dishImage : [self.viewModel defaultDishImage];
    self.dishName.text = [self.viewModel dishName];
    NSArray *ingredients =[self.viewModel dishIngredients];
    NSMutableArray *ingredientsArray = [[NSMutableArray alloc] init];
    self.ingredientImg1.image = nil;
    self.ingredientImg2.image = nil;
    self.ingredientImg3.image = nil;
    self.ingredientImg4.image = nil;
    self.ingredientImg5.image = nil;
    int i=0;
    for (IngredientDish *ingredientDish in ingredients) {
        if(![ingredientsArray containsObject:ingredientDish.ingredient.type.name]){
            i++;
            [ingredientsArray addObject:ingredientDish.ingredient.type.name];
            UIImage *imgIngredient = [UIImage imageNamed:[NSString stringWithFormat:@"i-%@",ingredientDish.ingredient.type.name]];
            switch (i) {
                case 1:
                    self.ingredientImg1.image = imgIngredient;
                    break;
                case 2:
                    self.ingredientImg2.image = imgIngredient;
                    break;
                case 3:
                    self.ingredientImg3.image = imgIngredient;
                    break;
                case 4:
                    self.ingredientImg4.image = imgIngredient;
                    break;
                case 5:
                    self.ingredientImg5.image = imgIngredient;
                    break;
                default:
                    break;
            }
        }
    }
    
}

- (void)setupView {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editDish:)];
    [self.editButton setUserInteractionEnabled:YES];
    [self.editButton addGestureRecognizer:tap];
    
    self.dishName.font = CUSTOM_FONT_NORMAL_LABEL;
    
}

- (void)editDish:(id)sender {
    [self.delegate openDishEditorWithViewModel:self.viewModel];
    
}



@end
