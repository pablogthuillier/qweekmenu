//
//  PGTDishTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 17/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dish+Model.h"
#import "PGTDishViewModel.h"

@interface PGTDishTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *dishImageView;
@property (weak, nonatomic) IBOutlet UILabel *dishNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg1;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg2;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg3;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg4;
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg5;


@property (nonatomic,strong) PGTDishViewModel *viewModel;


@end
