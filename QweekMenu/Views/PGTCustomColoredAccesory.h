//
//  PGTCustomColoredAccesory.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 05/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTCustomColoredAccesory : UIControl

@property (nonatomic, retain) UIColor *accessoryColor;
@property (nonatomic, retain) UIColor *highlightedColor;

+ (instancetype)accessoryWithColor:(UIColor *)color;

@end
