//
//  PGTDishTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 17/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTDishTableViewCell.h"
#import "Ingredient+Model.h"
#import "IngredientType+Model.h"
#import "IngredientDish.h"



@implementation PGTDishTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setViewModel:(PGTDishViewModel *)viewModel{
    _viewModel = viewModel;
    [self loadData];
    [self setupView];
    
    
}

- (void)loadData {
    self.dishNameTextField.text = [self.viewModel dishName];
    self.dishImageView.image = self.dishImageView.image = self.viewModel.dishImage ? self.viewModel.dishImage : [self.viewModel defaultDishImage];
    NSArray *ingredients = [self.viewModel dishIngredients];
    NSMutableArray *ingredientsArray = [[NSMutableArray alloc] init];
    int i=0;
    self.ingredientImg1.image = nil;
    self.ingredientImg2.image = nil;
    self.ingredientImg3.image = nil;
    self.ingredientImg4.image = nil;
    self.ingredientImg5.image = nil;
    for (IngredientDish *ingredientDish in ingredients) {
        if(![ingredientsArray containsObject:ingredientDish.ingredient.type.name]){
            i++;
            [ingredientsArray addObject:ingredientDish.ingredient.type.name];
            UIImage *imgIngredient = [UIImage imageNamed:[NSString stringWithFormat:@"i-%@",ingredientDish.ingredient.type.name]];
            switch (i) {
                case 1:
                    self.ingredientImg1.image = imgIngredient;
                    break;
                case 2:
                    self.ingredientImg2.image = imgIngredient;
                    break;
                case 3:
                    self.ingredientImg3.image = imgIngredient;
                    break;
                case 4:
                    self.ingredientImg4.image = imgIngredient;
                    break;
                case 5:
                    self.ingredientImg5.image = imgIngredient;
                    break;
                default:
                    break;
            }
        }
    }

}

- (void)setupView {
    
    self.dishImageView.layer.cornerRadius = 30;
    [self.dishImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.dishImageView setClipsToBounds:YES];
    
    self.dishNameTextField.font = CUSTOM_FONT_NORMAL_LABEL;
   
}


@end
