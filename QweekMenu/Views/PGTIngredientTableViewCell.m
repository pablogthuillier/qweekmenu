//
//  PGTIngredientTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 24/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTIngredientTableViewCell.h"
#import "IngredientType+Model.h"

@interface PGTIngredientTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *ingredientImg;
@property (weak, nonatomic) IBOutlet UILabel *ingredientLbl;

@end

@implementation PGTIngredientTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setViewModel:(PGTIngredientViewModel *)viewModel{
    
    _viewModel = viewModel;
    
    self.ingredientLbl.text = [viewModel ingredientName];
    self.ingredientLbl.font = CUSTOM_FONT_NORMAL_LABEL;

    self.ingredientImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"i-%@",[viewModel ingredientType]]];
    
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
