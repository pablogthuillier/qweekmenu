//
//  PGTFeedbackDishesView.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 03/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTFeedbackDishesView.h"

@interface PGTFeedbackDishesView ()
@property (weak, nonatomic) IBOutlet UIImageView *arrowLeft;
@property (weak, nonatomic) IBOutlet UILabel *centralText;
@property (weak, nonatomic) IBOutlet UIImageView *arrowRight;

@property (weak, nonatomic) IBOutlet UILabel *swippingText;
@property (weak, nonatomic) IBOutlet UIImageView *swippingArrow;
@end

@implementation PGTFeedbackDishesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)show:(BOOL)showValue animated:(BOOL)animated{
    
    self.arrowLeft.hidden = NO;
    self.centralText.hidden = NO;
    self.arrowRight.hidden = NO;

    
    CGFloat originalArrowAlpha = 0.3;
    
    self.arrowLeft.alpha = 0;
    self.centralText.alpha = 0;
    self.arrowRight.alpha = 0;
    
    if(animated){
        [UIView animateWithDuration:2 animations:^{
            self.arrowLeft.alpha = originalArrowAlpha;
            self.arrowRight.alpha = originalArrowAlpha;
            self.centralText.alpha = 1;
        } completion:^(BOOL finished) {
        }];
    }
    
    else{
        self.arrowLeft.alpha = originalArrowAlpha;
        self.arrowRight.alpha = originalArrowAlpha;
        self.centralText.alpha = 1;
    }
    
    self.hidden = !showValue;
    
}


- (void)setupWithTip1:(NSString *)tip1 andTip2:(NSString *)tip2 {
    
    self.hidden = YES;
    
    self.arrowLeft.hidden = NO;
    self.arrowRight.hidden = NO;
    self.centralText.hidden = NO;
    
    self.swippingText.hidden = NO;
    self.swippingArrow.hidden = NO;

    
    self.centralText.font = CUSTOM_FONT_NORMAL_LABEL;
    self.swippingText.font = CUSTOM_FONT_NORMAL_LABEL;
    
    
    if(tip1){
        self.centralText.text = tip1;
    }
    else{
        self.arrowLeft.hidden = YES;
        self.arrowRight.hidden = YES;
        
    }
    
    if(tip2){
        self.swippingText.text = tip2;
    }
    else{
        self.swippingArrow.hidden = YES;
        self.swippingText.hidden = YES;
    }
    
}

@end
