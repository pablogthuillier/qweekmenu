//
//  PGTEditDishTableViewCell.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 01/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGTIngredientDishViewModel.h"

@interface PGTEditDishTableViewCell : UITableViewCell

@property (nonatomic,strong) PGTIngredientDishViewModel *viewModel;

@end
