//
//  PGTFeedbackDishesView.h
//  QweekMenu
//
//  Created by Pablo González Thuillier on 03/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PGTFeedbackDishesView : UIView

- (void)show:(BOOL)showValue animated:(BOOL)animated;
- (void)setupWithTip1:(NSString *)tip1 andTip2:(NSString *)tip2;

@end
