//
//  PGTImportedDishTableViewCell.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 03/08/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTImportedDishTableViewCell.h"


@interface PGTImportedDishTableViewCell ()


@end

@implementation PGTImportedDishTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.dishName.font = CUSTOM_FONT_SMALL_LABEL;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
