//
//  PGTFeedbackView.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 22/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTFeedbackView.h"

@interface PGTFeedbackView ()
@property (weak, nonatomic) IBOutlet UIImageView *arrowRandom;
@property (weak, nonatomic) IBOutlet UIImageView *arrowDishes;
@property (weak, nonatomic) IBOutlet UILabel *labelDishes;
@property (weak, nonatomic) IBOutlet UILabel *labelRandom;

@end

@implementation PGTFeedbackView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    
    
}

- (void)show:(BOOL)showValue animated:(BOOL)animated{
    
    self.arrowRandom.hidden = NO;
    self.labelRandom.hidden = NO;
    self.arrowDishes.hidden = NO;
    self.labelDishes.hidden = NO;
    
    CGFloat originalArrowRandomAlpha = 0.3;
    CGFloat originalArrowDishesAlpha = 0.3;
    
    self.arrowRandom.alpha = 0;
    self.labelRandom.alpha = 0;
    
    self.arrowDishes.alpha = 0;
    self.labelDishes.alpha = 0;
    
    if(animated){
        [UIView animateWithDuration:2 animations:^{
            self.arrowDishes.alpha = originalArrowDishesAlpha;
            self.labelDishes.alpha = 1;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:2 animations:^{
                if(finished){
                    self.arrowRandom.alpha = originalArrowRandomAlpha;
                    self.labelRandom.alpha = 1;
                }
                
            } completion:^(BOOL finished) {
                
            }];
        }];
    }
    
    else{
        self.arrowDishes.alpha = originalArrowDishesAlpha;
        self.labelDishes.alpha = 1;
        self.arrowRandom.alpha = originalArrowRandomAlpha;
        self.labelRandom.alpha = 1;
    }

    self.hidden = !showValue;
}


- (void)setupWithTip1:(NSString *)tip1 andTip2:(NSString *)tip2 {
    
    self.arrowRandom.hidden = NO;
    self.labelRandom.hidden = NO;
    self.arrowDishes.hidden = NO;
    self.labelDishes.hidden = NO;
    
    
    self.labelDishes.font = CUSTOM_FONT_NORMAL_LABEL;
    self.labelRandom.font = CUSTOM_FONT_NORMAL_LABEL;
    
    
    if(tip1){
        self.labelDishes.text = tip1;
    }
    else{
        self.arrowDishes.hidden = YES;
        self.labelDishes.hidden = YES;
        
    }
    
    if(tip2){
        self.labelRandom.text = tip2;
    }
    else{
        self.arrowRandom.hidden = YES;
        self.labelRandom.hidden = YES;
    }
    
}


@end
