//
//  PGTAppDelegate.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//

#import "PGTAppDelegate.h"
#import "DishDatabaseAvailability.h"
#import "PGTWeekTableViewController.h"
#import "PGTDishesTableViewController.h"
#import "Dish+Model.h"
#import "PGTDocument.h"
#import "PGTDateManager.h"


@interface PGTAppDelegate ()

@property (nonatomic,strong) PGTDocument *managedDocument;

@end

@implementation PGTAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [self initializeVC];
    [self applicationApparence];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self sendNotificationForDatabaseAvailability];
    NSDate *lastDate = [[NSUserDefaults standardUserDefaults] objectForKey:SETTINGS_LAST_DATE];
    NSDate *actualDate = [PGTDateManager getZeroHourFromDate:[NSDate date]];
    if( [actualDate timeIntervalSinceDate:lastDate] > 0 ){
        [self sendNotificationForDateChange];
    }


}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma mark - Core Data stack


- (PGTDocument *)managedDocument
{
    if(!_managedDocument){
        NSURL *fileURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Qweek.data"];
        _managedDocument = [[PGTDocument alloc] initWithFileURL:fileURL];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]]) {
            [_managedDocument openWithCompletionHandler:^(BOOL success){
                if (!success) {
                    // Handle the error.
                }
            }];
        }
        else {
            [_managedDocument saveToURL:fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success){
                [self insertInitialData];
                if (!success) {
                    // Handle the error.
                }
            }];
        }
        
    }
    
    return _managedDocument;
}

- (void)insertInitialData {
    NSString *pathFileIngredients = [[NSBundle mainBundle]pathForResource:@"InitialIngredients" ofType:@"plist"];
    NSArray *arrayAuxIngredients = [NSMutableArray arrayWithContentsOfFile:pathFileIngredients];
    for(NSDictionary *dict in arrayAuxIngredients){
        if([dict isKindOfClass:[NSDictionary class]]){
            [self.managedDocument.managedObjectContext.undoManager beginUndoGrouping];
            [Ingredient ingredientInMOC:self.managedDocument.managedObjectContext withDictionary:dict];
            [self.managedDocument.managedObjectContext.undoManager endUndoGrouping];
        }
        
    }
    
    NSNumber *avoidLegumesAtNight = @1;
    NSNumber *maxMeat = @4;
    NSNumber *maxCereals = @4;
    
    [[NSUserDefaults standardUserDefaults] setObject:avoidLegumesAtNight forKey:SETTINGS_LEGUMES_NAME];
    [[NSUserDefaults standardUserDefaults] setObject:maxMeat forKey:SETTINGS_MEAT_MAX_NAME];
    [[NSUserDefaults standardUserDefaults] setObject:maxCereals forKey:SETTINGS_CEREALS_MAX_NAME];
    
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Initializate ViewControllers


- (void)sendNotificationForDatabaseAvailability {
    NSDictionary *userInfo = @{DishDatabaseAvailabilityDocument : self.managedDocument};
    [[NSNotificationCenter defaultCenter] postNotificationName:DishDatabaseAvailabilityNotification
                                                        object:self
                                                      userInfo:userInfo];
    
}

- (void)sendNotificationForDateChange {
    [[NSNotificationCenter defaultCenter] postNotificationName:DATE_CHANGE_NOTIFICATION
                                                        object:self
                                                      userInfo:nil];
}

- (void)initializeVC {
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    UINavigationController *mainNavController = (UINavigationController *)tabBarController.viewControllers[0];
    PGTWeekTableViewController *mainController = (PGTWeekTableViewController *)mainNavController.topViewController;
    mainController.managedDocument = self.managedDocument;
    
    UINavigationController *dishesNavController = (UINavigationController *)tabBarController.viewControllers[1];
    PGTWeekTableViewController *dishesController = (PGTWeekTableViewController *)dishesNavController.topViewController;
    dishesController.managedDocument = self.managedDocument;
    

}

- (void)applicationApparence {
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithHexString:COLOR_DARK_BROWN]];
    tabBarController.view.tintColor = [UIColor colorWithHexString:COLOR_DARK_BROWN];
    
//    UIImage *imgWhite = [UIImage imageNamed:@"white_pattern"];
//    
//    
//    [[UINavigationBar appearance] setBackgroundImage:imgWhite forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];

}

@end
