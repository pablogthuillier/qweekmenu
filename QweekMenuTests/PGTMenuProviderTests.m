//
//  PGTMenuProviderTests.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 19/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//


#import <XCTest/XCTest.h>
//#import <OCMock/OCMock.h>
#import "PGTMenuProvider.h"
#import "Dish+Model.h"
#import "Ingredient+Model.h"


@interface PGTMenuProviderTests : XCTestCase {
    // Core Data stack objects.
    NSManagedObjectModel *model;
    NSPersistentStoreCoordinator *coordinator;
    NSPersistentStore *store;
    NSManagedObjectContext *context;
    // Object to test.
    PGTMenuProvider *sut;
    Dish *dish;
    NSMutableArray *dishes;
    NSMutableArray *ocurrences;
    
    
}

@end


@implementation PGTMenuProviderTests

#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];

    [self createCoreDataStack];
    [self createFixture];
    [self createSut];
}


- (void) createCoreDataStack {
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    model = [NSManagedObjectModel mergedModelFromBundles:@[bundle]];
    coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    store = [coordinator addPersistentStoreWithType: NSInMemoryStoreType
                                      configuration: nil
                                                URL: nil
                                            options: nil
                                              error: NULL];
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = coordinator;
}


- (void) createFixture {
    // Test data
    dish = [Dish createDishInMoc:context];
    
    Dish *dish2 = [Dish createDishInMoc:context];

    
    dishes = [[NSMutableArray alloc] init];
    
    [dishes addObject:dish];
    [dishes addObject:dish2];
    
    Dish *dishOcurrence = [Dish createDishInMoc:context];

    
    ocurrences = [[NSMutableArray alloc] init];
    
    [ocurrences addObject:dishOcurrence];
    
    
}


- (void) createSut {
    sut = [[PGTMenuProvider alloc] init];
}


- (void) tearDown {
    [self releaseSut];
    [self releaseFixture];
    [self releaseCoreDataStack];

    [super tearDown];
}


- (void) releaseSut {
    sut = nil;
}


- (void) releaseFixture {
    dish = nil;

}


- (void) releaseCoreDataStack {
    context = nil;
    store = nil;
    coordinator = nil;
    model = nil;
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    // Prepare

    // Operate

    // Check
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}

- (void)testDish1HaveNoRepeatedIngredientsWithDish2 {
    
    BOOL result = [PGTMenuProvider dish:dish hasRepeatedIngredientsWithDish:ocurrences[0]];
    XCTAssertFalse(result, @"Dish1 has repeated ingredients with Dish2");
    
}

@end
