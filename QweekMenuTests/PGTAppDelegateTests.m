//
//  PGTAppDelegateTests.m
//  QweekMenu
//
//  Created by Pablo González Thuillier on 14/07/14.
//  Copyright (c) 2014 Thuillier. All rights reserved.
//


#import <XCTest/XCTest.h>
//#import <OCMock/OCMock.h>
#import "PGTAppDelegate.h"


@interface MocFake : NSManagedObjectContext

@property (assign) BOOL saveWasCalled;

@end

@implementation MocFake

- (BOOL) hasChanges {
    return YES;
}

- (BOOL) save:(NSError *__autoreleasing *)error {
    self.saveWasCalled = YES;
    return YES;
}

@end


@interface PGTAppDelegateTests : XCTestCase {
    // Object to test.
    PGTAppDelegate *sut;
}

@end


@implementation PGTAppDelegateTests

#pragma mark - Set up and tear down

- (void) setUp {
    [super setUp];
    
    [self createSut];
}


- (void) createSut {
    sut = [[PGTAppDelegate alloc] init];
}


- (void) tearDown {
    [self releaseSut];
    
    [super tearDown];
}


- (void) releaseSut {
    sut = nil;
}


#pragma mark - Basic test

- (void) testObjectIsNotNil {
    // Prepare
    
    // Operate
    
    // Check
    XCTAssertNotNil(sut, @"The object to test must be created in setUp.");
}


#pragma mark - Core Data stack




@end